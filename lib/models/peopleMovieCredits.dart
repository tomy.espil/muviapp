import 'package:muvi_app/models/movieModel.dart';

class PeopleCredits {
  List<Movies> cast;
  List<Movies> crew;
  int id;

  PeopleCredits({this.cast, this.crew, this.id});

  PeopleCredits.fromJson(Map<String, dynamic> json) {
    if (json['cast'] != null) {
      cast = new List<Movies>();
      json['cast'].forEach((v) {
        cast.add(new Movies.fromJson(v));
      });
    }
    if (json['crew'] != null) {
      crew = new List<Movies>();
      json['crew'].forEach((v) {
        crew.add(new Movies.fromJson(v));
      });
    }
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.cast != null) {
      data['cast'] = this.cast.map((v) => v.toJson()).toList();
    }
    if (this.crew != null) {
      data['crew'] = this.crew.map((v) => v.toJson()).toList();
    }
    data['id'] = this.id;
    return data;
  }
}

class Cast {
  String character;
  String creditId;
  String posterPath;
  int id;
  bool video;
  int voteCount;
  bool adult;
  String backdropPath;
  List<int> genreIds;
  String originalLanguage;
  String originalTitle;
  double popularity;
  String title;
  int voteAverage;
  String overview;
  String releaseDate;

  Cast(
      {this.character,
      this.creditId,
      this.posterPath,
      this.id,
      this.video,
      this.voteCount,
      this.adult,
      this.backdropPath,
      this.genreIds,
      this.originalLanguage,
      this.originalTitle,
      this.popularity,
      this.title,
      this.voteAverage,
      this.overview,
      this.releaseDate});

  Cast.fromJson(Map<String, dynamic> json) {
    character = json['character'];
    creditId = json['credit_id'];
    posterPath = json['poster_path'];
    id = json['id'];
    video = json['video'];
    voteCount = json['vote_count'];
    adult = json['adult'];
    backdropPath = json['backdrop_path'];
    genreIds = json['genre_ids'].cast<int>();
    originalLanguage = json['original_language'];
    originalTitle = json['original_title'];
    popularity = json['popularity'];
    title = json['title'];
    voteAverage = json['vote_average'];
    overview = json['overview'];
    releaseDate = json['release_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['character'] = this.character;
    data['credit_id'] = this.creditId;
    data['poster_path'] = this.posterPath;
    data['id'] = this.id;
    data['video'] = this.video;
    data['vote_count'] = this.voteCount;
    data['adult'] = this.adult;
    data['backdrop_path'] = this.backdropPath;
    data['genre_ids'] = this.genreIds;
    data['original_language'] = this.originalLanguage;
    data['original_title'] = this.originalTitle;
    data['popularity'] = this.popularity;
    data['title'] = this.title;
    data['vote_average'] = this.voteAverage;
    data['overview'] = this.overview;
    data['release_date'] = this.releaseDate;
    return data;
  }
}

class Crew {
  int id;
  String department;
  String originalLanguage;
  String originalTitle;
  String job;
  String overview;
  int voteCount;
  bool video;
  String releaseDate;
  /* double voteAverage; */
  String title;
  double popularity;
  List<int> genreIds;
  String backdropPath;
  bool adult;
  String posterPath;
  String creditId;

  Crew(
      {this.id,
      this.department,
      this.originalLanguage,
      this.originalTitle,
      this.job,
      this.overview,
      this.voteCount,
      this.video,
      this.releaseDate,
      /* this.voteAverage, */
      this.title,
      this.popularity,
      this.genreIds,
      this.backdropPath,
      this.adult,
      this.posterPath,
      this.creditId});

  Crew.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    department = json['department'];
    originalLanguage = json['original_language'];
    originalTitle = json['original_title'];
    job = json['job'];
    overview = json['overview'];
    voteCount = json['vote_count'];
    video = json['video'];
    releaseDate = json['release_date'];
    /* voteAverage = json['vote_average']; */
    title = json['title'];
    popularity = json['popularity'];
    genreIds = json['genre_ids'].cast<int>();
    backdropPath = json['backdrop_path'];
    adult = json['adult'];
    posterPath = json['poster_path'];
    creditId = json['credit_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['department'] = this.department;
    data['original_language'] = this.originalLanguage;
    data['original_title'] = this.originalTitle;
    data['job'] = this.job;
    data['overview'] = this.overview;
    data['vote_count'] = this.voteCount;
    data['video'] = this.video;
    data['release_date'] = this.releaseDate;
    /* data['vote_average'] = this.voteAverage; */
    data['title'] = this.title;
    data['popularity'] = this.popularity;
    data['genre_ids'] = this.genreIds;
    data['backdrop_path'] = this.backdropPath;
    data['adult'] = this.adult;
    data['poster_path'] = this.posterPath;
    data['credit_id'] = this.creditId;
    return data;
  }
}