class Tmdb{
  static const apiKey = '78d2f21bc794d87df835cacd50bd10e3';
  static const urlSimple = 'https://api.themoviedb.org/3/';
  static const baseUrl = 'https://api.themoviedb.org/3/movie/';
  static const baseUrlTv = 'https://api.themoviedb.org/3/tv/';
  static const baseImageUrl = 'https://image.tmdb.org/t/p/';
  static const personUrl = 'https://api.themoviedb.org/3/person/';
  static const basePeopleUrl = 'https://api.themoviedb.org/3/person/';

  static const nowPlayingUrl = '${baseUrl}now_playing?api_key=$apiKey&language=es&region=AR';
  static const upComingUrl = '${baseUrl}upcoming?api_key=$apiKey&language=es&region=AR';
  static const popularUrl = '${baseUrl}popular?api_key=$apiKey&language=es';
  static const topRatedUrl = '${baseUrl}top_rated?api_key=$apiKey&language=es&region=AR';

  static const discoverMovieRevenueUrl = '${urlSimple}discover/movie?api_key=$apiKey&language=es&sort_by=revenue.desc&include_adult=false&include_video=false&page=1';

  static const tvAiringTodayUrl = '${baseUrlTv}airing_today?api_key=$apiKey&language=es';
  //static const tvLatestUrl = '${baseUrlTv}latest?api_key=$apiKey&language=es';
  static const tvOnTheAirUrl = '${baseUrlTv}on_the_air?api_key=$apiKey&language=es';
  static const tvPopularUrl = '${baseUrlTv}popular?api_key=$apiKey&language=es&page=1';
  static const tvTopRatedUrl = '${baseUrlTv}top_rated?api_key=$apiKey&language=es';

  static const discoverTvSeriesPopularityUrl = '${urlSimple}discover/tv?api_key=$apiKey&language=es&page=1&timezone=America%2FBuenos_Aires&include_null_first_air_dates=false';

  static const genresMoviesUrl = '${urlSimple}genre/movie/list?api_key=$apiKey&language=es';

  static const searchUrl = 'https://api.themoviedb.org/3/search/multi?api_key=$apiKey&language=es&query=';

  static const String actionCode = '28';
  static const String adventureCode = '12';
  static const String animacionCode = '16';
  static const String comediaCode = '35';
  static const String crimenCode = '80';
  static const String documentalCode = '99';
  static const String dramaCode = '18';
  static const String familiaCode = '10751';
  static const String fantasiaCode = '14';
  static const String historiaCode = '36';
  static const String terrorCode = '27';
  static const String musicaCode = '10402';
  static const String misterioCode = '9648';
  static const String romanceCode = '10749';
  static const String cienciaFiccionCode = '878';
  static const String deTvCode = '10770';
  static const String suspensoCode = '53';
  static const String belicaCode = '10752';
  static const String westernCode = '37';
  

}