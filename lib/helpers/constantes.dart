class Constantes {
  static const String Settings = 'Configuracion';
  static const String Rated = 'Valorar';
  static const String SignOut = 'Cerrar Sesion';

  static const List<String> choices = <String>[
    Settings,
    Rated,
    SignOut,
  ];
}