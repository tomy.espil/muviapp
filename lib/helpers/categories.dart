class Categories {
  static const Popularity_desc = 'Popularidad descendente';
  static const Popularity_asce = 'Popularidad ascendente';
  static const Estreno_desc = 'Estreno descendente';
  static const Estreno_asce = 'Estreno ascendente';
  static const Alfabetico_desc = 'Alfabetico descendente';
  static const Alfabetico_asce = 'Alfabetico ascendente';
  static const Valoracion_desc = 'Valoracion descendente';
  static const Valoracion_asce = 'Valoracion ascendente';
  static const Votos_desc = 'Votos descendente';
  static const Votos_asce = 'Votos ascendente';


  static List<String> choices = <String>[
    Popularity_desc,
    Popularity_asce,
    Estreno_desc,
    Estreno_asce,
    Alfabetico_desc,
    Alfabetico_asce,
    Valoracion_desc,
    Valoracion_asce,
    Votos_desc,
    Votos_asce

  ];
}