import 'package:flutter/material.dart';
import 'package:muvi_app/pages/extendListPage.dart';
import 'package:muvi_app/widgets/list_Items.dart';

class CardsList extends StatelessWidget {
  final typeItem;
  final String listTitle;
  final String codeList;
  final String sortBy;
  final String year;
  final bool loading;

  CardsList(
      {this.typeItem,
      this.listTitle,
      this.codeList,
      this.sortBy,
      this.year,
      this.loading});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 270.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 10.0, bottom: 4.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: 30.0,
                      width: 1.0,
                      color: Color.fromRGBO(255, 0, 57, 1),
                    ),
                    SizedBox(
                      width: 12.0,
                    ),
                    Text(
                      listTitle,
                      style: TextStyle(
                          fontSize: 17.0,
                          fontFamily: 'MontserratMedium',
                          color: Colors.grey[200]),
                      textScaleFactor: 1.0,
                    ),
                  ],
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ExtendListPage(
                                listName: listTitle,
                                codeListMovies: codeList,
                                sortBy: sortBy,
                                year: year,
                              )));
                },
                child: Padding(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: Text(
                    'Ver todas >',
                    style: TextStyle(
                        fontFamily: 'RalewayMedium',
                        fontSize: 15.0,
                        color: Colors.white30),
                  ),
                ),
              ),
            ],
          ),
          Flexible(
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: typeItem == null || loading == true
                  ? <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 38.0),
                      child: Center(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          CircularProgressIndicator(
                            valueColor: new AlwaysStoppedAnimation<Color>(
                                Color.fromRGBO(255, 0, 57, 1)),
                          ),
                          SizedBox(
                            height: 3.0,
                          ),
                          Text(
                            'Cargando..',
                            style: TextStyle(
                                fontFamily: 'RalewayMedium',
                                color: Colors.white,
                                fontSize: 16.0),
                          )
                        ],
                      )),
                    )
                  ]
                  : typeItem.movies
                      .map<Widget>((movieItem) => Padding(
                            padding: EdgeInsets.only(
                                top: 6.0, left: 6.0, right: 2.0),
                            child: ListItems(
                                item: movieItem, loading: loading),
                          ))
                      .toList(),
            ),
          )
        ],
      ),
    );
  }
}
