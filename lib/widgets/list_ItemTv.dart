import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:muvi_app/models/tmdb.dart';
import 'package:muvi_app/pages/tvDetails_page.dart';

class ListItemTv extends StatelessWidget {
  final itemTv;
  final bool loading;

  ListItemTv({this.itemTv, this.loading});

  final dt = DateFormat('yyyy');

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Padding(
          padding: const EdgeInsets.only(top: 6.0, bottom: 6.0),
          child: Column(
            children: <Widget>[
              Card(
                elevation: 5.0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0)),
                child: Container(
                    height: 180.0,
                    width: 120.0,
                    child: Stack(children: <Widget>[
                      new ClipRRect(
                        borderRadius: new BorderRadius.circular(4.0),
                        child: InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => TvDetailPage(
                                            tvSerie: itemTv,
                                          )));
                            },
                            child: itemTv.posterPath == null
                                ? Container(
                                    height: 180.0,
                                    width: double.infinity,
                                    color: Color.fromRGBO(35, 47, 52, 1),
                                    child: Icon(
                                      Icons.live_tv,
                                      color: Colors.grey,
                                    ))
                                : Image.network(
                                    '${Tmdb.baseImageUrl}w185${itemTv.posterPath}',
                                    fit: BoxFit.cover,
                                    width: double.infinity,
                                    height: 200.0,
                                  )),
                      ),
                      Positioned(
                          top: 3.0,
                          right: 3.0,
                          child: itemTv.firstAirDate == null ||
                                  itemTv.firstAirDate == ""
                              ? Container(
                                  height: 15.0,
                                  width: 30.0,
                                  decoration: BoxDecoration(
                                      color: Color.fromRGBO(255, 0, 57, .9),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(7.5))),
                                  child: Center(
                                      child: Text("s/d",
                                          style: TextStyle(
                                            fontSize: 13.0,
                                            fontFamily: 'RalewayLight',
                                            color: Colors.grey,
                                          ))))
                              : Container(
                                  height: 15.0,
                                  width: 30.0,
                                  decoration: BoxDecoration(
                                      color: Color.fromRGBO(255, 0, 57, .9),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(7.5))),
                                  child: Center(
                                      child: Text(
                                    "${dt.format(DateTime.tryParse(itemTv.firstAirDate))}",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: 'RalewayMedium',
                                      fontSize: 11.0,
                                    ),
                                    textAlign: TextAlign.center,
                                  )),
                                ))
                    ])),
              ),
              Padding(
                padding: EdgeInsets.all(5.0),
                child: Container(
                  width: 110.0,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Center(
                        child: Text(
                          itemTv.name,
                          style: TextStyle(
                              fontFamily: 'MontserratRegular',
                              fontSize: 10.0,
                              color: Colors.white70),
                          textScaleFactor: 1.0,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.center,
                        ),
                      ),
                      SizedBox(
                        height: 3.0,
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );
  }
}