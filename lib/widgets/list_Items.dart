import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:muvi_app/models/movieModel.dart';
import 'package:muvi_app/models/tmdb.dart';
import 'package:muvi_app/pages/movieDetails_page.dart';

class ListItems extends StatelessWidget {
final item;
final bool loading;

final dt = DateFormat('yyyy');

ListItems({this.item, this.loading});

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Padding(
          padding: const EdgeInsets.only(top: 6.0, bottom: 6.0),
          child: Column(
            children: <Widget>[
              Card(
                elevation: 10.0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                child: Container(
                    height: 180.0,
                    width: 120.0,
                    child: Stack(children: <Widget>[
                      new ClipRRect(
                        borderRadius: new BorderRadius.circular(7.0),
                        child: InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => MovieDetailPage(
                                            movie: item,
                                          )));
                            },
                            child: item.posterPath == null ||
                                    loading == true
                                ? Container(
                                    height: 180.0,
                                    width: double.infinity,
                                    color: Color.fromRGBO(35, 47, 52, 1),
                                    child: Icon(
                                      Icons.movie,
                                      color: Colors.grey,
                                    ))
                                : Image.network(
                                    '${Tmdb.baseImageUrl}w342${item.posterPath}',
                                    fit: BoxFit.cover,
                                    width: double.infinity,
                                    height: 200.0,
                                  )),
                      ),
                      Positioned(
                          top: 3.0,
                          right: 3.0,
                          child: item.releaseDate == null ||
                                  item.releaseDate == ""
                              ? Container(
                                  height: 15.0,
                                  width: 30.0,
                                  decoration: BoxDecoration(
                                      color: Color.fromRGBO(255, 0, 57, .9),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(7.5))),
                                  child: Center(
                                      child: Text(
                                    "s/d",
                                    style: TextStyle(
                                      fontSize: 11.0,
                                      fontFamily: 'RalewayLight',
                                      color: Colors.grey,
                                    ),
                                    textScaleFactor: 1.0,
                                  )),
                                )
                              : Container(
                                  height: 15.0,
                                  width: 30.0,
                                  decoration: BoxDecoration(
                                      color: Color.fromRGBO(255, 0, 57, .9),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(7.5))),
                                  child: Center(
                                    child: Text(
                                      "${dt.format(DateTime.tryParse(item.releaseDate))}",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: 'RalewayMedium',
                                        fontSize: 10.0,
                                      ),
                                      textAlign: TextAlign.center,
                                      textScaleFactor: 1.0,
                                    ),
                                  ),
                                ))
                    ])),
              ),
              Padding(
                padding: EdgeInsets.only(top: 4.0),
                child: Container(
                  width: 110.0,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Center(
                        child: Text(
                          item.title,
                          style: TextStyle(
                              fontFamily: 'MontserratRegular',
                              fontSize: 10.0,
                              color: Colors.white70),
                          textScaleFactor: 1.0,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );
  }
}