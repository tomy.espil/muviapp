import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

import 'package:muvi_app/helpers/constantes.dart';
import 'package:muvi_app/models/movieDetailModel.dart';
import 'package:muvi_app/models/peopleTrending.dart';
import 'package:muvi_app/models/tmdb.dart';
import 'package:muvi_app/models/movieModel.dart';
import 'package:muvi_app/pages/movieDetails_page.dart';
import 'package:muvi_app/pages/nowPlayingMovies.dart';
import 'package:muvi_app/pages/peopleDetails_page.dart';
import 'package:muvi_app/pages/popularMovies.dart';
import 'package:muvi_app/pages/topRatedMovies.dart';
import 'package:muvi_app/pages/tvSeries_page.dart';
import 'package:muvi_app/pages/upCommingMovies.dart';
import 'package:muvi_app/widgets/cards_List.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePage createState() => new _HomePage();
}

class _HomePage extends State<HomePage> with SingleTickerProviderStateMixin {
  Movie movieTrending;
  Movie action;
  Movie adventure;
  Movie animation;
  Movie comedia;
  Movie crimen;
  Movie documental;
  Movie drama;
  Movie familia;
  Movie fantasia;
  Movie historia;
  Movie terror;
  Movie musica;
  Movie misterio;
  Movie romance;
  Movie cienciaFiccion;
  Movie peliculasDeTv;
  Movie suspenso;
  Movie belicas;
  Movie western;
  Movie de2019;
  Movie de2018;
  Movie de2017;
  Movie de2016;
  Movie de2015;

  TabController tabController;
  bool darkThemeEnabled = true;
  MovieDetailModel movieDetails;
  PeopleTrending peopleTrending;

  String peopleTrendingUrl;
  String movieTrendingUrl;
  String actionUrl;
  String adventureUrl;
  String animationUrl;
  String comediaUrl;
  String crimenUrl;
  String documentalUrl;
  String dramaUrl;
  String familiaUrl;
  String fantasiaUrl;
  String historiaUrl;
  String terrorUrl;
  String musicaUrl;
  String misterioUrl;
  String romanceUrl;
  String cienciaFiccionUrl;
  String peliculasDeTvUrl;
  String suspensoUrl;
  String belicasUrl;
  String westernUrl;
  String de2019Url;
  String de2018Url;
  String de2017Url;
  String de2016Url;
  String de2015Url;

  final dt = DateFormat('yyyy');

  bool isLoading = true;

  @override
  void initState() {
    super.initState();
    peopleTrendingUrl =
        '${Tmdb.urlSimple}trending/person/day?api_key=${Tmdb.apiKey}';
    movieTrendingUrl =
        '${Tmdb.urlSimple}trending/movie/day?api_key=${Tmdb.apiKey}';
    actionUrl =
        '${Tmdb.urlSimple}discover/movie?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&with_genres=${Tmdb.actionCode}';
    adventureUrl =
        '${Tmdb.urlSimple}discover/movie?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&with_genres=${Tmdb.adventureCode}';
    animationUrl =
        '${Tmdb.urlSimple}discover/movie?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&with_genres=${Tmdb.animacionCode}';
    comediaUrl =
        '${Tmdb.urlSimple}discover/movie?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&with_genres=${Tmdb.comediaCode}';
    crimenUrl =
        '${Tmdb.urlSimple}discover/movie?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&with_genres=${Tmdb.crimenCode}';
    documentalUrl =
        '${Tmdb.urlSimple}discover/movie?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&with_genres=${Tmdb.documentalCode}';
    dramaUrl =
        '${Tmdb.urlSimple}discover/movie?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&with_genres=${Tmdb.dramaCode}';
    familiaUrl =
        '${Tmdb.urlSimple}discover/movie?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&with_genres=${Tmdb.familiaCode}';
    fantasiaUrl =
        '${Tmdb.urlSimple}discover/movie?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&with_genres=${Tmdb.fantasiaCode}';
    historiaUrl =
        '${Tmdb.urlSimple}discover/movie?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&with_genres=${Tmdb.historiaCode}';
    terrorUrl =
        '${Tmdb.urlSimple}discover/movie?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&with_genres=${Tmdb.terrorCode}';
    musicaUrl =
        '${Tmdb.urlSimple}discover/movie?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&with_genres=${Tmdb.musicaCode}';
    misterioUrl =
        '${Tmdb.urlSimple}discover/movie?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&with_genres=${Tmdb.misterioCode}';
    romanceUrl =
        '${Tmdb.urlSimple}discover/movie?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&with_genres=${Tmdb.romanceCode}';
    cienciaFiccionUrl =
        '${Tmdb.urlSimple}discover/movie?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&with_genres=${Tmdb.cienciaFiccionCode}';
    peliculasDeTvUrl =
        '${Tmdb.urlSimple}discover/movie?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&with_genres=${Tmdb.deTvCode}';
    suspensoUrl =
        '${Tmdb.urlSimple}discover/movie?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&with_genres=${Tmdb.suspensoCode}';
    belicasUrl =
        '${Tmdb.urlSimple}discover/movie?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&with_genres=${Tmdb.belicaCode}';
    westernUrl =
        '${Tmdb.urlSimple}discover/movie?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&with_genres=${Tmdb.westernCode}';
    de2019Url =
        '${Tmdb.urlSimple}discover/movie?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&primary_release_year=2019';
    de2018Url =
        '${Tmdb.urlSimple}discover/movie?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&primary_release_year=2018';
    de2017Url =
        '${Tmdb.urlSimple}discover/movie?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&primary_release_year=2017';
    de2016Url =
        '${Tmdb.urlSimple}discover/movie?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&primary_release_year=2016';
    de2015Url =
        '${Tmdb.urlSimple}discover/movie?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&primary_release_year=2015';
    _fetchPeopleTrending();
    _fetchMovieTrending();
    _fetchAction();
    _fetchAdventure();
    _fetchAnimation();
    _fetchComedia();
    _fetchCrimen();
    _fetchDocumental();
    _fetchDrama();
    _fetchFantasia();
    _fetchFamilia();
    _fetchHistoria();
    _fetchTerror();
    _fetchMusica();
    _fetchMisterio();
    _fetchRomance();
    _fetchCienciaFiccion();
    _fetchDeTv();
    _fetchSuspenso();
    _fetchBelica();
    _fetchWestern();
    _fetch2019();
    _fetch2018();
    _fetch2017();
    _fetch2016();
    _fetch2015();
    isLoading = false;
    tabController = new TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  void _fetchPeopleTrending() async {
    var response = await http.get(peopleTrendingUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      peopleTrending = PeopleTrending.fromJson(decodeJson);
    });
  }

  void _fetchMovieTrending() async {
    var response = await http.get(movieTrendingUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      movieTrending = Movie.fromJson(decodeJson);
    });
  }

  void _fetchAction() async {
    var response = await http.get(actionUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      action = Movie.fromJson(decodeJson);
    });
  }

  void _fetchAdventure() async {
    var response = await http.get(adventureUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      adventure = Movie.fromJson(decodeJson);
    });
  }

  void _fetchAnimation() async {
    var response = await http.get(animationUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      animation = Movie.fromJson(decodeJson);
    });
  }

  void _fetchComedia() async {
    var response = await http.get(comediaUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      comedia = Movie.fromJson(decodeJson);
    });
  }

  void _fetchCrimen() async {
    var response = await http.get(crimenUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      crimen = Movie.fromJson(decodeJson);
    });
  }

  void _fetchDocumental() async {
    var response = await http.get(documentalUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      documental = Movie.fromJson(decodeJson);
    });
  }

  void _fetchDrama() async {
    var response = await http.get(dramaUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      drama = Movie.fromJson(decodeJson);
    });
  }

  void _fetchFamilia() async {
    var response = await http.get(familiaUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      familia = Movie.fromJson(decodeJson);
    });
  }

  void _fetchFantasia() async {
    var response = await http.get(fantasiaUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      fantasia = Movie.fromJson(decodeJson);
    });
  }

  void _fetchHistoria() async {
    var response = await http.get(historiaUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      historia = Movie.fromJson(decodeJson);
    });
  }

  void _fetchTerror() async {
    var response = await http.get(terrorUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      terror = Movie.fromJson(decodeJson);
    });
  }

  void _fetchMusica() async {
    var response = await http.get(musicaUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      musica = Movie.fromJson(decodeJson);
    });
  }

  void _fetchMisterio() async {
    var response = await http.get(misterioUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      misterio = Movie.fromJson(decodeJson);
    });
  }

  void _fetchRomance() async {
    var response = await http.get(romanceUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      romance = Movie.fromJson(decodeJson);
    });
  }

  void _fetchCienciaFiccion() async {
    var response = await http.get(cienciaFiccionUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      cienciaFiccion = Movie.fromJson(decodeJson);
    });
  }

  void _fetchDeTv() async {
    var response = await http.get(peliculasDeTvUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      peliculasDeTv = Movie.fromJson(decodeJson);
    });
  }

  void _fetchSuspenso() async {
    var response = await http.get(suspensoUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      suspenso = Movie.fromJson(decodeJson);
    });
  }

  void _fetchBelica() async {
    var response = await http.get(belicasUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      belicas = Movie.fromJson(decodeJson);
    });
  }

  void _fetchWestern() async {
    var response = await http.get(westernUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      western = Movie.fromJson(decodeJson);
    });
  }

  void _fetch2019() async {
    var response = await http.get(de2019Url);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      de2019 = Movie.fromJson(decodeJson);
    });
  }

  void _fetch2018() async {
    var response = await http.get(de2018Url);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      de2018 = Movie.fromJson(decodeJson);
    });
  }

  void _fetch2017() async {
    var response = await http.get(de2017Url);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      de2017 = Movie.fromJson(decodeJson);
    });
  }

  void _fetch2016() async {
    var response = await http.get(de2016Url);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      de2016 = Movie.fromJson(decodeJson);
    });
  }

  void _fetch2015() async {
    var response = await http.get(de2015Url);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      de2015 = Movie.fromJson(decodeJson);
    });
  }

  Widget _buildPeopleTrendingList() => Container(
        height: 300.0,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 10.0, bottom: 4.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    height: 30.0,
                    width: 1.0,
                    color: Color.fromRGBO(255, 0, 57, 1),
                  ),
                  SizedBox(
                    width: 12.0,
                  ),
                  Text(
                    'Celebridades del momento',
                    style: TextStyle(
                        fontSize: 17.0,
                        fontFamily: 'MontserratMedium',
                        color: Colors.grey[200]),
                    textScaleFactor: 1.0,
                  ),
                ],
              ),
            ),
            Flexible(
              child: ListView(
                  padding:
                      const EdgeInsets.only(top: 10.0, left: 12.0, bottom: 3.0),
                  scrollDirection: Axis.horizontal,
                  children: peopleTrending == null || isLoading == true
                      ? <Widget>[
                          Center(
                              child: CircularProgressIndicator(
                            backgroundColor: Colors.green,
                          ))
                        ]
                      : peopleTrending.results
                          .map((p) => Padding(
                              padding: EdgeInsets.only(right: 10.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Stack(children: <Widget>[
                                    InkWell(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    PeoplePage(
                                                      person: p.id,
                                                    )));
                                      },
                                      child: Container(
                                        height: 220.0,
                                        width: 150.0,
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(7.0),
                                          child: p.profilePath != null
                                              ? Image.network(
                                                  '${Tmdb.baseImageUrl}h632${p.profilePath}',
                                                  fit: BoxFit.cover,
                                                )
                                              : Image.asset(
                                                  'assets/nobody.jpg',
                                                  fit: BoxFit.cover,
                                                ),
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      bottom: 8.0,
                                      left: 2.0,
                                      child: Container(
                                        width: 140.0,
                                        child: Text(
                                          p.name,
                                          style: TextStyle(
                                            fontFamily: 'MontserrarMedium',
                                            color: Colors.white,
                                            fontSize: 18.0,
                                          ),
                                          textScaleFactor: 1.0,
                                          overflow: TextOverflow.ellipsis,
                                          textAlign: TextAlign.center,
                                          maxLines: 2,
                                        ),
                                      ),
                                    )
                                  ]),
                                ],
                              )))
                          .toList()),
            )
          ],
        ),
      );

  Widget _buildMovieListItemCard(Movies movieItem) => Container(
        child: Padding(
          padding: const EdgeInsets.only(top: 6.0, bottom: 6.0),
          child: Column(
            children: <Widget>[
              Card(
                elevation: 10.0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                child: Container(
                    height: 200.0,
                    width: MediaQuery.of(context).size.width * .9,
                    child: Stack(children: <Widget>[
                      new ClipRRect(
                        borderRadius: new BorderRadius.circular(7.0),
                        child: InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => MovieDetailPage(
                                            movie: movieItem,
                                          )));
                            },
                            child: movieItem.backdropPath == null ||
                                    isLoading == true
                                ? Container(
                                    height: 180.0,
                                    width: double.infinity,
                                    color: Color.fromRGBO(35, 47, 52, 1),
                                    child: Icon(
                                      Icons.movie,
                                      color: Colors.grey,
                                    ))
                                : Image.network(
                                    '${Tmdb.baseImageUrl}w780${movieItem.backdropPath}',
                                    fit: BoxFit.cover,
                                    width: double.infinity,
                                    height: 200.0,
                                  )),
                      ),
                      Positioned(
                          top: 3.0,
                          right: 3.0,
                          child: movieItem.releaseDate == null ||
                                  movieItem.releaseDate == ""
                              ? Container(
                                  height: 15.0,
                                  width: 30.0,
                                  decoration: BoxDecoration(
                                      color: Color.fromRGBO(255, 0, 57, .9),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(7.5))),
                                  child: Center(
                                      child: Text(
                                    "s/d",
                                    style: TextStyle(
                                      fontSize: 11.0,
                                      fontFamily: 'RalewayLight',
                                      color: Colors.grey,
                                    ),
                                    textScaleFactor: 1.0,
                                  )),
                                )
                              : Container(
                                  height: 15.0,
                                  width: 30.0,
                                  decoration: BoxDecoration(
                                      color: Color.fromRGBO(255, 0, 57, .9),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(7.5))),
                                  child: Center(
                                    child: Text(
                                      "${dt.format(DateTime.tryParse(movieItem.releaseDate))}",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: 'RalewayMedium',
                                        fontSize: 10.0,
                                      ),
                                      textAlign: TextAlign.center,
                                      textScaleFactor: 1.0,
                                    ),
                                  ),
                                )),
                      Positioned(
                        bottom: 15.0,
                        left: 15.0,
                        child: Container(
                          width: MediaQuery.of(context).size.width * .8,
                          child: Text(
                            movieItem.title,
                            style: TextStyle(
                                fontFamily: 'MontserratMedium',
                                fontSize: 20.0,
                                color: Colors.white),
                            textScaleFactor: 1.0,
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.center,
                            maxLines: 2,
                          ),
                        ),
                      ),
                    ])),
              ),
            ],
          ),
        ),
      );

  Widget _buildCardsListCard(Movie movie, String movieListTitle,
          String codeList, String sortBy, String year) =>
      Container(
        height: 265.0,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 4.0, bottom: 4.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: 30.0,
                        width: 1.0,
                        color: Color.fromRGBO(255, 0, 57, 1),
                      ),
                      SizedBox(
                        width: 6.0,
                      ),
                      Text(
                        movieListTitle,
                        style: TextStyle(
                            fontSize: 17.0,
                            fontFamily: 'MontserratMedium',
                            color: Colors.grey[200]),
                        textScaleFactor: 1.0,
                      ),
                    ],
                  ),
                )
              ],
            ),
            Flexible(
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: movie == null || isLoading == true
                    ? <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left: 38.0),
                          child: Center(
                              child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              CircularProgressIndicator(
                                valueColor: new AlwaysStoppedAnimation<Color>(
                                    Color.fromRGBO(255, 0, 57, 1)),
                              ),
                              SizedBox(
                                height: 3.0,
                              ),
                              Text(
                                'Cargando..',
                                style: TextStyle(
                                    fontFamily: 'RalewayMedium',
                                    color: Colors.white,
                                    fontSize: 16.0),
                              )
                            ],
                          )),
                        )
                      ]
                    : movie.movies
                        .map((movieItem) => Padding(
                              padding: EdgeInsets.only(left: 6.0, right: 2.0),
                              child: _buildMovieListItemCard(movieItem),
                            ))
                        .toList(),
              ),
            )
          ],
        ),
      );

  Widget _buildTopContent() => Padding(
        padding:
            EdgeInsets.only(top: 18.0, left: 18.0, right: 18.0, bottom: 10.0),
        child: Container(
          height: MediaQuery.of(context).size.height * 0.36,
          width: double.infinity,
          color: Colors.transparent,
          child: Column(
            children: <Widget>[
              Text(
                'Explora y descubre las maravillas del séptimo arte y la televisión.',
                style: TextStyle(
                  fontFamily: 'MontserratBold',
                  fontSize: 26.0,
                  color: Colors.white,
                ),
                textAlign: TextAlign.left,
                textScaleFactor: 1.0,
              ),
              SizedBox(
                height: 20.0,
              ),
              Container(
                width: double.infinity,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    MaterialButton(
                      elevation: 5.0,
                      color: Color.fromRGBO(255, 0, 57, 1),
                      child: Text(
                        'CARTELERA',
                        style: TextStyle(
                            fontFamily: 'RalewayBold',
                            fontSize: 14.0,
                            color: Colors.white),
                        textScaleFactor: 1.0,
                      ),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => NowPlayingMovies()));
                      },
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    OutlineButton(
                      borderSide:
                          BorderSide(color: Color.fromRGBO(255, 0, 57, 1)),
                      child: Text(
                        'PROXIMOS ESTRENOS',
                        style: TextStyle(
                            fontFamily: 'RalewayBold',
                            fontSize: 14.0,
                            color: Colors.white),
                        textScaleFactor: 1.0,
                      ),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => UpCommingMovies()));
                      },
                    )
                  ],
                ),
              ),
              Container(
                child: Row(
                  children: <Widget>[
                    OutlineButton(
                      borderSide:
                          BorderSide(color: Color.fromRGBO(255, 0, 57, 1)),
                      child: Text(
                        'POPULARES',
                        style: TextStyle(
                            fontFamily: 'RalewayBold',
                            fontSize: 14.0,
                            color: Colors.white),
                        textScaleFactor: 1.0,
                      ),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => PopularMovies()));
                      },
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    MaterialButton(
                      elevation: 5.0,
                      color: Color.fromRGBO(255, 0, 57, 1),
                      child: Text(
                        'MEJOR PUNTUADAS',
                        style: TextStyle(
                            fontFamily: 'RalewayBold',
                            fontSize: 14.0,
                            color: Colors.white),
                        textScaleFactor: 1.0,
                      ),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => TopRatedMovies()));
                      },
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      );

  Widget _buildViewPage() => ListView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          _buildTopContent(),
          SizedBox(
            height: 20.0,
          ),
          _buildCardsListCard(movieTrending, 'Peliculas de momento', '', '', ''),
          SizedBox(
            height: 30.0,
          ),
          CardsList(
              typeItem: de2019, listTitle: 'Películas de 2019', codeList: '', sortBy: 'release_date.asc', year: '2019', loading: isLoading),
          SizedBox(
            height: 30.0,
          ),
          CardsList(
              typeItem: action, listTitle: 'Acción', codeList: Tmdb.actionCode, sortBy: 'popularity.desc', year: '', loading: isLoading),
          SizedBox(
            height: 30.0,
          ),
          CardsList(
              typeItem: adventure, listTitle: 'Aventura', codeList: Tmdb.adventureCode, sortBy: 'popularity.desc', year: '', loading: isLoading),
          SizedBox(
            height: 30.0,
          ),
          _buildPeopleTrendingList(),
          SizedBox(
            height: 30.0,
          ),
          CardsList(
              typeItem: belicas, listTitle: 'Bélicas', codeList: Tmdb.belicaCode, sortBy: 'popularity.desc', year: '', loading: isLoading),
          SizedBox(
            height: 30.0,
          ),
          CardsList(
              typeItem: de2018, listTitle: 'Películas de 2018', codeList: '', sortBy: 'popularity.desc', year: '2018', loading: isLoading),
          SizedBox(
            height: 30.0,
          ),
          CardsList(
              typeItem: animation, listTitle: 'Animación', codeList: Tmdb.animacionCode, sortBy: 'popularity.desc', year: '', loading: isLoading),
          SizedBox(
            height: 30.0,
          ),
          CardsList(
              typeItem: cienciaFiccion, listTitle: 'Ciencia ficción', codeList: Tmdb.cienciaFiccionCode, sortBy: 'popularity.desc', year: '', loading: isLoading),
          SizedBox(
            height: 30.0,
          ),
          CardsList(
              typeItem:comedia, listTitle: 'Comedia', codeList: Tmdb.comediaCode, sortBy: 'popularity.desc', year: '', loading: isLoading),
          SizedBox(
            height: 30.0,
          ),
          CardsList(
              typeItem: de2017, listTitle: 'Películas de 2017', codeList: '', sortBy: 'popularity.desc', year: '2017', loading: isLoading),
          SizedBox(
            height: 30.0,
          ),
          CardsList(
              typeItem: crimen, listTitle: 'Crimen', codeList: Tmdb.crimenCode, sortBy: 'popularity.desc', year: '', loading: isLoading),
          SizedBox(
            height: 30.0,
          ),
          CardsList(
              typeItem: documental, listTitle: 'Documentales', codeList: Tmdb.documentalCode, sortBy: 'popularity.desc', year: '', loading: isLoading),
          SizedBox(
            height: 30.0,
          ),
          CardsList(
              typeItem: drama, listTitle: 'Drama', codeList: Tmdb.dramaCode, sortBy: 'popularity.desc', year: '', loading: isLoading),
          SizedBox(
            height: 30.0,
          ),
          CardsList(
              typeItem: familia, listTitle: 'Família', codeList: Tmdb.familiaCode, sortBy: 'popularity.desc', year: '', loading: isLoading),
          SizedBox(
            height: 30.0,
          ),
          CardsList(
              typeItem: de2016, listTitle: 'Películas de 2016', codeList: '', sortBy: 'popularity.desc', year: '2016', loading: isLoading),
          SizedBox(
            height: 30.0,
          ),
          CardsList(
              typeItem: fantasia, listTitle: 'Fantasía', codeList: Tmdb.fantasiaCode, sortBy: 'popularity.desc', year: '', loading: isLoading),
          SizedBox(
            height: 30.0,
          ),
          CardsList(
              typeItem: historia, listTitle: 'Historia', codeList: Tmdb.historiaCode, sortBy: 'popularity.desc', year: '', loading: isLoading),
          SizedBox(
            height: 30.0,
          ),
          CardsList(
              typeItem: misterio, listTitle: 'Misterio', codeList: Tmdb.misterioCode, sortBy: 'popularity.desc', year: '', loading: isLoading),
          SizedBox(
            height: 30.0,
          ),
          CardsList(
              typeItem: musica, listTitle: 'Música', codeList: Tmdb.musicaCode, sortBy: 'popularity.desc', year: '', loading: isLoading),
          SizedBox(
            height: 30.0,
          ),
          CardsList(
              typeItem: de2015, listTitle: 'Películas de 2015', codeList: '', sortBy: 'popularity.desc', year: '2015', loading: isLoading),
          SizedBox(
            height: 30.0,
          ),
          CardsList(
              typeItem: peliculasDeTv, listTitle: 'Películas de Tv', codeList: Tmdb.deTvCode, sortBy: 'popularity.desc', year: '', loading: isLoading),
          SizedBox(
            height: 30.0,
          ),
          CardsList(
              typeItem: romance, listTitle: 'Romance', codeList: Tmdb.romanceCode, sortBy: 'popularity.desc', year: '', loading: isLoading),
          SizedBox(
            height: 30.0,
          ),
          CardsList(
              typeItem: suspenso, listTitle: 'Suspenso', codeList: Tmdb.suspensoCode, sortBy: 'popularity.desc', year: '', loading: isLoading),
          SizedBox(
            height: 30.0,
          ),
          CardsList(
              typeItem: terror, listTitle: 'Terror', codeList: Tmdb.terrorCode, sortBy: 'popularity.desc', year: '', loading: isLoading),
          SizedBox(
            height: 30.0,
          ),
          CardsList(
              typeItem: western, listTitle: 'Western', codeList: Tmdb.westernCode, sortBy: 'popularity.desc', year: '', loading: isLoading),
        ],
      );

  void choiseAction(String choise) {}

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: new Container(
        child: Scaffold(
          backgroundColor: Color.fromRGBO(26, 27, 31, 1),
          appBar: AppBar(
            backgroundColor: Color.fromRGBO(18, 19, 23, 1),
            elevation: 0.0,
            centerTitle: true,
            title: Text(
              'Muvi App',
              style: TextStyle(
                fontFamily: 'MontserratMedium',
                fontSize: 22.0,
              ),
              textScaleFactor: 1.0,
            ),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.search),
                onPressed: () {},
              ),
              PopupMenuButton<String>(
                  onSelected: choiseAction,
                  itemBuilder: (BuildContext context) {
                    return Constantes.choices.map((String choices) {
                      return PopupMenuItem<String>(
                          value: choices, child: Text(choices));
                    }).toList();
                  }),
            ],
          ),
          body: TabBarView(
            children: <Widget>[
              _buildViewPage(),
              new TvSeriesPage(),
            ],
            controller: tabController,
          ),
          drawer: new Drawer(
            elevation: 5.0,
            child: Container(
              color: Color.fromRGBO(18, 19, 23, 1),
              child: ListView(
                scrollDirection: Axis.vertical,
                children: <Widget>[
                  SizedBox(
                    height: 30.0,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(width: 20.0),
                      CircleAvatar(
                        maxRadius: 35.0,
                        backgroundImage: NetworkImage(
                            'https://images.pexels.com/photos/555790/pexels-photo-555790.png?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'),
                      ),
                      SizedBox(
                        width: 25.0,
                      ),
                      Column(
                        children: <Widget>[
                          Text(
                            'Tomy Espil',
                            style: TextStyle(
                                fontFamily: 'RalewayBold',
                                fontSize: 16.0,
                                color: Colors.white),
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Text(
                            'tomy.espil@gmail.com',
                            style: TextStyle(
                                fontFamily: 'MontserratRegular',
                                fontSize: 14.0,
                                color: Color.fromRGBO(69, 73, 79, 1),
                                fontStyle: FontStyle.italic),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 25.0,
                  ),
                  Divider(),
                  SizedBox(
                    height: 25.0,
                  ),
                  Text(
                    'CONFIGURACIONES',
                    style: TextStyle(
                        fontFamily: 'MontserratMedium',
                        color: Colors.white,
                        fontSize: 17.0),
                    textAlign: TextAlign.center,
                  ),
                  ListTile(
                    title: Text(
                      'Tema oscuro',
                      style: TextStyle(
                          fontFamily: 'MontserratMedium',
                          fontSize: 16.0,
                          color: Color.fromRGBO(69, 73, 79, 1)),
                    ),
                    trailing: Switch(
                      value: darkThemeEnabled,
                      onChanged: (changeTheme) {
                        setState(() {
                          darkThemeEnabled = changeTheme;
                        });
                      },
                    ),
                  ),
                  ListTile(
                    title: Text(
                      'Notificaciones',
                      style: TextStyle(
                          fontFamily: 'MontserratMedium',
                          fontSize: 16.0,
                          color: Color.fromRGBO(69, 73, 79, 1)),
                    ),
                    trailing: Switch(
                      value: false,
                      onChanged: (value) {
                        setState(() {});
                      },
                    ),
                  ),
                  ExpansionTile(
                    title: Text(
                      'Idioma',
                      style: TextStyle(
                          fontFamily: 'MontserratMedium',
                          fontSize: 16.0,
                          color: Color.fromRGBO(69, 73, 79, 1)),
                    ),
                    children: <Widget>[
                      FlatButton(
                        child: Text(
                          'Español',
                          style: TextStyle(
                              fontFamily: 'MontserratMedium',
                              fontSize: 16.0,
                              color: Color.fromRGBO(69, 73, 79, 1)),
                        ),
                        onPressed: () {},
                      ),
                      FlatButton(
                        child: Text(
                          'Inglés',
                          style: TextStyle(
                              fontFamily: 'MontserratMedium',
                              fontSize: 16.0,
                              color: Color.fromRGBO(69, 73, 79, 1)),
                        ),
                        onPressed: () {},
                      ),
                      FlatButton(
                        child: Text(
                          'Portugués',
                          style: TextStyle(
                              fontFamily: 'MontserratMedium',
                              fontSize: 16.0,
                              color: Color.fromRGBO(69, 73, 79, 1)),
                        ),
                        onPressed: () {},
                      ),
                      FlatButton(
                        child: Text(
                          'Francés',
                          style: TextStyle(
                              fontFamily: 'MontserratMedium',
                              fontSize: 16.0,
                              color: Color.fromRGBO(69, 73, 79, 1)),
                        ),
                        onPressed: () {},
                      ),
                    ],
                  ),
                  Divider(),
                  SizedBox(
                    height: 25.0,
                  ),
                  Text(
                    'AYUDANOS',
                    style: TextStyle(
                        fontFamily: 'MontserratMedium',
                        color: Colors.white,
                        fontSize: 17.0),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 8.0,
                  ),
                  FlatButton(
                    child: Text(
                      'Valoranos',
                      style: TextStyle(
                          fontFamily: 'MontserratMedium',
                          fontSize: 16.0,
                          color: Color.fromRGBO(69, 73, 79, 1)),
                    ),
                    onPressed: () {},
                  )
                ],
              ),
            ),
          ),
          bottomNavigationBar: new TabBar(
            unselectedLabelColor: Color.fromRGBO(69, 73, 79, 1),
            indicatorColor: Color.fromRGBO(255, 0, 57, .8),
            labelColor: Color.fromRGBO(255, 0, 57, .8),
            labelStyle: TextStyle(fontFamily: 'RalewayMedium', fontSize: 17.0),
            labelPadding: EdgeInsets.all(0.0),
            controller: tabController,
            tabs: <Widget>[
              new Tab(
                text: 'PELICULAS',
              ),
              new Tab(
                text: 'SERIES',
              ),
            ],
          ),
        ),
      ),
    );
  }
}
