import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class TmdbPage extends StatefulWidget {
  final String idTmdb;
  final String name;

  TmdbPage({this.idTmdb, this.name});

  _TmdbPageState createState() => _TmdbPageState();
}

class _TmdbPageState extends State<TmdbPage> {
  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(18, 19, 23, 1),
        title: Text(
          'TMDb - ${widget.name}',
          style: TextStyle(
            fontSize: 18.0,
            color: Colors.white,
            fontFamily: 'MontserratMedium',
          ),
        ),
      ),
      withZoom: true,
      url: 'https://www.themoviedb.org/${widget.idTmdb}',
    );
  }
}
