import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class VideosWebViewPage extends StatefulWidget {
  final String urlVideo;

  VideosWebViewPage({this.urlVideo});

  _VideosWebViewPageState createState() => _VideosWebViewPageState();
}

class _VideosWebViewPageState extends State<VideosWebViewPage> {
  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      supportMultipleWindows: true,
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(18, 19, 23, 1),
        title: Text(
          'Youtube',
          style: TextStyle(
            fontSize: 18.0,
            color: Colors.white,
            fontFamily: 'MontserratMedium',
          ),
        ),
      ),
      withZoom: true,
      url: 'https://youtu.be/${widget.urlVideo}',
    );
  }
}