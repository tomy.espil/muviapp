import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:muvi_app/helpers/categories.dart';
import 'package:muvi_app/helpers/loader.dart';
import 'package:muvi_app/models/movieModel.dart';
import 'package:muvi_app/models/tmdb.dart';
import 'package:muvi_app/pages/movieDetails_page.dart';

class ExtendListPage extends StatefulWidget {
  final String listName;
  final String codeListMovies;
  final String sortBy;
  final String year;

  ExtendListPage({this.listName, this.codeListMovies, this.sortBy, this.year});

  _ExtendListPageState createState() => _ExtendListPageState();
}

class _ExtendListPageState extends State<ExtendListPage> {
  Movie listMovies;

  int _pageNumber = 1;
  int _totalItems = 3;

  String moviesSortBy;

  @override
  void initState() {
    super.initState();
    _fetchListMoviesMovies();
  }

  void _fetchListMoviesMovies() async {
    var response = await http.get(
        '${Tmdb.urlSimple}discover/movie?api_key=${Tmdb.apiKey}&language=es&sort_by=${widget.sortBy}&include_adult=false&include_video=false&page=$_pageNumber&primary_release_year=${widget.year}&with_genres=${widget.codeListMovies}');
    var decodeJson = jsonDecode(response.body);
    listMovies == null
        ? listMovies = Movie.fromJson(decodeJson)
        : listMovies.movies.addAll(Movie.fromJson(decodeJson).movies);
    if (!mounted) return;
    setState(() {
      _totalItems = listMovies.movies.length;
    });
  }

  Widget _buildMovieListItem(Movies movieItem) => Container(
        height: 170.0,
        child: Padding(
          padding: const EdgeInsets.only(left: 10.0, right: 10.0, top: 15.0),
          child: Stack(
            children: <Widget>[
              Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                      child: Container(
                        height: 1.0,
                        width: double.infinity,
                        color: Color.fromRGBO(255, 0, 57, .8),
                      ),
                    ),
                    Card(
                      color: Color.fromRGBO(14, 15, 18, 1),
                      elevation: 6.0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      child: Stack(
                        children: <Widget>[
                          Container(
                            height: 110.0,
                            child: InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => MovieDetailPage(
                                              movie: movieItem,
                                            )));
                              },
                            ),
                          ),
                          Positioned(
                              left: 120.0,
                              child: Container(
                                width: 190.0,
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      movieItem.title,
                                      style: TextStyle(
                                          fontFamily: 'MontserratBold',
                                          fontSize: 15.0,
                                          color: Colors.white),
                                      overflow: TextOverflow.ellipsis,
                                      textScaleFactor: 1.0,
                                    ),
                                    SizedBox(
                                      height: 7.0,
                                    ),
                                    Container(
                                      child: movieItem.releaseDate == null
                                          ? 'Sin datos'
                                          : Text(
                                              "${DateFormat('yyyy').format(DateTime.parse(movieItem.releaseDate))}",
                                              style: TextStyle(
                                                fontSize: 13.0,
                                                fontFamily: 'RalewayLight',
                                                color: Colors.grey,
                                              ),
                                              textScaleFactor: 1.0,),
                                    ),
                                    SizedBox(
                                      height: 7.0,
                                    ),
                                    Container(
                                        child: movieItem.overview == ""
                                            ? Text(
                                                'Sinopsis desconocida',
                                                style: TextStyle(
                                                  fontSize: 13.0,
                                                  fontFamily: 'RalewayLight',
                                                  color: Colors.white70,
                                                ),
                                                textScaleFactor: 1.0,
                                              )
                                            : Text(
                                                movieItem.overview,
                                                style: TextStyle(
                                                  fontSize: 13.0,
                                                  fontFamily: 'RalewayLight',
                                                  color: Colors.white,
                                                ),
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 3,
                                                textScaleFactor: 1.0,
                                              ))
                                  ],
                                ),
                              ))
                        ],
                      ),
                    ),
                  ]),
              Positioned(
                left: 15.0,
                bottom: 12.0,
                child: Container(
                  height: 140.0,
                  width: 100.0,
                  child: ClipRRect(
                    borderRadius: new BorderRadius.circular(4.0),
                    child: movieItem.posterPath != null
                        ? Image.network(
                            '${Tmdb.baseImageUrl}w342${movieItem.posterPath}',
                            fit: BoxFit.cover,
                          )
                        : Image.asset(
                            'assets/emptyfilmposter.jpg',
                            fit: BoxFit.cover,
                          ),
                  ),
                ),
              ),
            ],
          ),
        ),
      );

  Widget _buildCardsList(Movie movie) => ListView.builder(
        itemCount: _totalItems,
        itemBuilder: (BuildContext context, int index) {
          if (index >= movie.movies.length - 1) {
            _pageNumber++;
            _fetchListMoviesMovies();
          }
          return Padding(
            padding: EdgeInsets.only(left: 4.0, right: 4.0),
            child: _buildMovieListItem(movie.movies[index]),
          );
        },
      );

  void choiseAction(String choise) {
    if (choise == Categories.Popularity_desc) {
      moviesSortBy = 'popularity.desc';
    } else if (choise == Categories.Popularity_asce) {
      moviesSortBy = 'popularity.asc';
    } else if (choise == Categories.Estreno_desc) {
      moviesSortBy = 'release_date.desc';
    } else if (choise == Categories.Estreno_asce) {
      moviesSortBy = 'release_date.asc';
    } else if (choise == Categories.Alfabetico_desc) {
      moviesSortBy = 'original_title.desc';
    } else if (choise == Categories.Alfabetico_asce) {
      moviesSortBy = 'original_title.asc';
    } else if (choise == Categories.Valoracion_desc) {
      moviesSortBy = 'vote_average.desc';
    } else if (choise == Categories.Valoracion_asce) {
      moviesSortBy = 'vote_average.asc';
    } else if (choise == Categories.Votos_desc) {
      moviesSortBy = 'vote_count.desc';
    } else if (choise == Categories.Votos_asce) {
      moviesSortBy = 'vote_count.asc';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(25, 26, 30, 1),
      appBar: new AppBar(
        backgroundColor: Color.fromRGBO(18, 19, 23, 1),
        elevation: 0.0,
        centerTitle: true,
        title: Text(
          widget.listName,
          style: TextStyle(
            fontFamily: 'RalewayMedium',
            fontSize: 20.0,
          ),
          textScaleFactor: 1.0,
        ),
        actions: <Widget>[
          PopupMenuButton<String>(
              onSelected: choiseAction,
              itemBuilder: (BuildContext context) {
                return Categories.choices.map((String choices) {
                  return PopupMenuItem<String>(
                      value: choices, child: Text(choices));
                }).toList();
              }),
        ],
      ),
      body: listMovies == null
          ? Center(
              child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Loader(),
                Text(
                  'Cargando peliculas de ${widget.listName}',
                  style: TextStyle(
                      fontFamily: 'RalewayMedium',
                      color: Colors.white,
                      fontSize: 16.0),
                )
              ],
            ))
          : _buildCardsList(listMovies),
    );
  }
}
