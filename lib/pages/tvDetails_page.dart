import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:muvi_app/helpers/loader.dart';
import 'package:muvi_app/models/movieTvCredits.dart';
import 'package:muvi_app/models/tmdb.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:muvi_app/models/tvDetailsModel.dart';
import 'package:muvi_app/models/tvSeriesModel.dart';
import 'package:muvi_app/pages/extendListPage.dart';
import 'package:muvi_app/pages/peopleDetails_page.dart';
import 'package:muvi_app/pages/seasonsPage.dart';

class TvDetailPage extends StatefulWidget {
  final Results tvSerie;

  TvDetailPage({this.tvSerie});

  @override
  _TvDetailPage createState() => new _TvDetailPage();
}

class _TvDetailPage extends State<TvDetailPage>
    with SingleTickerProviderStateMixin {
  String tvDetailUrl;
  String tvCreditsUrl;
  String similarTvUrl;
  String recommendedTvUrl;
  TvDetailsModel tvDetails;
  MovieTvCredits movieTvCredits;
  TvSeries similarTv;
  TvSeries recommendedTv;
  final oCcy = new NumberFormat("#,###", "es_AR");
  bool loading;
  TabController tabController;

  @override
  void initState() {
    super.initState();
    tvDetailUrl =
        '${Tmdb.baseUrlTv}${widget.tvSerie.id}?api_key=${Tmdb.apiKey}&language=Es';
    tvCreditsUrl =
        '${Tmdb.baseUrlTv}${widget.tvSerie.id}/credits?api_key=${Tmdb.apiKey}';
    similarTvUrl =
        '${Tmdb.baseUrlTv}${widget.tvSerie.id}/similar?api_key=${Tmdb.apiKey}&language=Es&page=1';
    recommendedTvUrl =
        '${Tmdb.baseUrlTv}${widget.tvSerie.id}/recommendations?api_key=${Tmdb.apiKey}&language=Es&page=1';
    _fetchTvDetails();
    _fetchTvCredits();
    _fetchSimilarTv();
    _fetchRecommendedTv();
    tabController = new TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  void _fetchTvDetails() async {
    var response = await http.get(tvDetailUrl);
    var decodedJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      tvDetails = TvDetailsModel.fromJson(decodedJson);
    });
  }

  void _fetchTvCredits() async {
    var response = await http.get(tvCreditsUrl);
    var decodedJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      movieTvCredits = MovieTvCredits.fromJson(decodedJson);
    });
  }

  void _fetchSimilarTv() async {
    var response = await http.get(similarTvUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      similarTv = TvSeries.fromJson(decodeJson);
    });
  }

  void _fetchRecommendedTv() async {
    var response = await http.get(recommendedTvUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      recommendedTv = TvSeries.fromJson(decodeJson);
    });
  }

  String _getTvPopularity(double popularity) {
    if (popularity == null) return 'Sin datos';
    return '$popularity';
  }

  Widget _buildCastContent() => Container(
        height: 220.0,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
            Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 30.0,
                  width: 1.0,
                  color: Color.fromRGBO(255, 0, 57, 1),
                ),
                SizedBox(
                  width: 6.0,
                ),
                Text('Reparto',
                    style: TextStyle(
                        fontSize: 20.0,
                        fontFamily: 'MontserratMedium',
                        color: Colors.grey[200])),
              ],
            ),
          ),
          Flexible(
            child: ListView(
              padding: EdgeInsets.only(left: 12.0, bottom: 4.0),
              scrollDirection: Axis.horizontal,
              children: movieTvCredits == null
                  ? <Widget>[
                      Center(
                          child: CircularProgressIndicator(
                        backgroundColor: Colors.green,
                      ))
                    ]
                  : movieTvCredits.cast
                      .map(
                        (c) => Padding(
                              padding: EdgeInsets.only(right: 10.0),
                              child: Column(
                                children: <Widget>[
                                  Stack(children: <Widget>[
                                    Container(
                                      child: InkWell(
                                          onTap: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        PeoplePage(
                                                            person: c.id)));
                                          },
                                          child: Container(
                                            height: 150.0,
                                            width: 100.0,
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(7.0),
                                              child: c.profilePath != null
                                                  ? Image.network(
                                                      '${Tmdb.baseImageUrl}h632${c.profilePath}',
                                                      fit: BoxFit.cover,
                                                    )
                                                  : Image.asset(
                                                      'assets/nobody.jpg',
                                                      fit: BoxFit.cover,
                                                    ),
                                            ),
                                          )),
                                    ),
                                    Positioned(
                                        bottom: 5.0,
                                        left: 2.0,
                                        child: Container(
                                          width: 90.0,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: <Widget>[
                                              Text(
                                                c.name,
                                                style: TextStyle(
                                                  fontFamily:
                                                      'MontserratMedium',
                                                  color: Colors.white,
                                                  fontSize: 12.0,
                                                ),
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 2,
                                                textAlign: TextAlign.center,
                                              ),
                                            ],
                                          ),
                                        ))
                                  ]),
                                  SizedBox(
                                    height: 4.0,
                                  ),
                                  Container(
                                    width: 90.0,
                                    child: Text(
                                      '"${c.character}"',
                                      style: TextStyle(
                                          fontSize: 11.0,
                                          fontFamily: 'MontserratRegular',
                                          color: Colors.white70,
                                          fontStyle: FontStyle.italic),
                                      overflow: TextOverflow.ellipsis,
                                      textAlign: TextAlign.center,
                                      maxLines: 2,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                      )
                      .toList(),
            ),
          )
        ]),
      );

  Widget _buildCrewContent() => Container(
        height: 220.0,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
            Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 30.0,
                  width: 1.0,
                  color: Color.fromRGBO(255, 0, 57, 1),
                ),
                SizedBox(
                  width: 6.0,
                ),
                Text('Produccion',
                    style: TextStyle(
                        fontSize: 20.0,
                        fontFamily: 'MontserratMedium',
                        color: Colors.grey[200])),
              ],
            ),
          ),
          Flexible(
            child: ListView(
              padding: EdgeInsets.only(left: 12.0, bottom: 4.0),
              scrollDirection: Axis.horizontal,
              children: movieTvCredits == null
                  ? <Widget>[
                      Center(
                          child: CircularProgressIndicator(
                        backgroundColor: Colors.green,
                      ))
                    ]
                  : movieTvCredits.crew
                      .map(
                        (c) => Padding(
                              padding: EdgeInsets.only(right: 10.0),
                              child: Column(
                                children: <Widget>[
                                  Stack(children: <Widget>[
                                    Container(
                                      child: InkWell(
                                          onTap: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        PeoplePage(
                                                            person: c.id)));
                                          },
                                          child: Container(
                                            height: 150.0,
                                            width: 100.0,
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(7.0),
                                              child: c.profilePath != null
                                                  ? Image.network(
                                                      '${Tmdb.baseImageUrl}h632${c.profilePath}',
                                                      fit: BoxFit.cover,
                                                    )
                                                  : Image.asset(
                                                      'assets/nobody.jpg',
                                                      fit: BoxFit.cover,
                                                    ),
                                            ),
                                          )),
                                    ),
                                    Positioned(
                                        bottom: 5.0,
                                        left: 2.0,
                                        child: Container(
                                          width: 90.0,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: <Widget>[
                                              Text(
                                                c.name,
                                                style: TextStyle(
                                                  fontFamily:
                                                      'MontserratMedium',
                                                  color: Colors.white,
                                                  fontSize: 12.0,
                                                ),
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 2,
                                                textAlign: TextAlign.center,
                                              ),
                                            ],
                                          ),
                                        ))
                                  ]),
                                  SizedBox(
                                    height: 4.0,
                                  ),
                                  Container(
                                    width: 90.0,
                                    child: Text(
                                      '"${c.job}"',
                                      style: TextStyle(
                                          fontSize: 11.0,
                                          fontFamily: 'MontserratRegular',
                                          color: Colors.white70,
                                          fontStyle: FontStyle.italic),
                                      overflow: TextOverflow.ellipsis,
                                      textAlign: TextAlign.center,
                                      maxLines: 2,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                      )
                      .toList(),
            ),
          )
        ]),
      );

  Widget _buildTopContent() => Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 7.0, right: 7.0),
              child: Container(
                height: 150.0,
                width: 100.0,
                alignment: FractionalOffset.center,
                child: ClipRRect(
                    borderRadius: new BorderRadius.circular(4.0),
                    child: widget.tvSerie.posterPath == null
                        ? Container(
                            height: 150.0,
                            width: 100.0,
                            color: Color.fromRGBO(35, 47, 52, 1),
                            child: Icon(
                              Icons.movie,
                              color: Colors.grey,
                            ))
                        : Image.network(
                            '${Tmdb.baseImageUrl}w342${widget.tvSerie.posterPath}',
                            fit: BoxFit.cover,
                            height: 150.0,
                            width: 100.0)),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 5.0),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.date_range,
                          color: Colors.white,
                          size: 20.0,
                        ),
                        SizedBox(
                          width: 7.0,
                        ),
                        Text(
                            tvDetails == null || tvDetails.firstAirDate == ""
                                ? "Sin informacion de estreno"
                                : "${DateFormat('dd.MM.yyyy').format(DateTime.parse(tvDetails.firstAirDate)).toString()}",
                            style: TextStyle(
                              fontSize: 14.0,
                              fontFamily: 'MontserratMedium',
                              color: Colors.white,
                            )),
                      ],
                    ),
                    SizedBox(
                      height: 3.0,
                    ),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.star,
                          color: Colors.white,
                          size: 20.0,
                        ),
                        SizedBox(
                          width: 5.0,
                        ),
                        Text(
                          tvDetails == null || tvDetails.voteAverage == 0.0
                              ? '-'
                              : '${tvDetails.voteAverage}/10 (${tvDetails.voteCount})',
                          style: TextStyle(
                              fontSize: 14.0,
                              fontFamily: 'MontserratMedium',
                              color: Colors.white),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 3.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.people,
                          color: Colors.white,
                          size: 20.0,
                        ),
                        SizedBox(
                          width: 5.0,
                        ),
                        Text(
                          tvDetails == null || tvDetails.popularity == 0.0
                              ? '-'
                              : _getTvPopularity(tvDetails.popularity),
                          style: TextStyle(
                              fontSize: 14.0,
                              fontFamily: 'MontserratMedium',
                              color: Colors.white),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 3.0,
                    ),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.list,
                          color: Colors.white,
                          size: 20.0,
                        ),
                        SizedBox(
                          width: 7.0,
                        ),
                        Text(
                          tvDetails == null && tvDetails.numberOfSeasons == 0
                              ? '-'
                              : '${tvDetails.numberOfSeasons.toString()} temporadas',
                          style: TextStyle(
                              fontSize: 14.0,
                              fontFamily: 'MontserratMedium',
                              color: Colors.white),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 3.0,
                    ),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.list,
                          color: Colors.white,
                          size: 20.0,
                        ),
                        SizedBox(
                          width: 7.0,
                        ),
                        Text(
                          tvDetails == null && tvDetails.numberOfEpisodes == 0
                              ? '-'
                              : '${tvDetails.numberOfEpisodes.toString()} episodios',
                          style: TextStyle(
                              fontSize: 14.0,
                              fontFamily: 'MontserratMedium',
                              color: Colors.white),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 3.0,
                    ),
                    /* Row(
                      children: <Widget>[
                        Icon(
                          Icons.language,
                          color: Colors.white,
                          size: 20.0,
                        ),
                        SizedBox(
                          width: 7.0,
                        ),
                        Container(
                            width: MediaQuery.of(context).size.width * .55,
                            child: tvDetails.originCountry == null ||
                                    tvDetails.originCountry == []
                                ? Text(
                                    'Sin datos',
                                    style: TextStyle(
                                        fontSize: 14.0,
                                        fontFamily: 'MontserratMedium',
                                        color: Colors.white),
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                  )
                                : Text(
                                    tvDetails.originCountry
                                        .map((c) => c)
                                        .toString(),
                                    style: TextStyle(
                                        fontSize: 14.0,
                                        fontFamily: 'MontserratMedium',
                                        color: Colors.white),
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                  ))
                      ],
                    ), */
                  ]),
            ),
          ]);

  Widget _buildOptions() => Padding(
        padding: EdgeInsets.only(right: 5.0, left: 5.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            MaterialButton(
              height: 50.0,
              minWidth: MediaQuery.of(context).size.width * .32,
              color: Color.fromRGBO(13, 13, 15, 1),
              child: Column(
                children: <Widget>[
                  Icon(
                    Icons.favorite_border,
                    color: Colors.white,
                  ),
                  Text(
                    'Favorita',
                    style: TextStyle(
                        fontFamily: 'RalewayRegular', color: Colors.white),
                  )
                ],
              ),
              onPressed: () {},
            ),
            MaterialButton(
              height: 50.0,
              minWidth: MediaQuery.of(context).size.width * .32,
              color: Color.fromRGBO(13, 13, 15, 1),
              child: Column(
                children: <Widget>[
                  Icon(
                    CupertinoIcons.add,
                    color: Colors.white,
                  ),
                  Text(
                    'Para ver',
                    style: TextStyle(
                        fontFamily: 'RalewayRegular', color: Colors.white),
                  )
                ],
              ),
              onPressed: () {},
            ),
            MaterialButton(
              height: 50.0,
              minWidth: MediaQuery.of(context).size.width * .32,
              color: Color.fromRGBO(13, 13, 15, 1),
              child: Column(
                children: <Widget>[
                  Icon(
                    CupertinoIcons.check_mark_circled,
                    color: Colors.white,
                  ),
                  Text(
                    'Vista',
                    style: TextStyle(
                        fontFamily: 'RalewayRegular', color: Colors.white),
                  )
                ],
              ),
              onPressed: () {},
            )
          ],
        ),
      );

  Widget _buildGenresList() => Container(
      height: 24.0,
      child: Padding(
        padding: const EdgeInsets.only(left: 14.0),
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: tvDetails.genres == null
              ? Container()
              : tvDetails.genres
                  .map((g) => Padding(
                        padding: const EdgeInsets.only(right: 6.0),
                        child: FilterChip(
                          backgroundColor: Color.fromRGBO(255, 0, 57, 1),
                          labelStyle: TextStyle(fontSize: 11.0),
                          label: Padding(
                            padding: const EdgeInsets.only(bottom: 8.0),
                            child: Text(g.name,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'RalewayBold',
                                    fontSize: 13.0)),
                          ),
                          onSelected: (b) {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ExtendListPage(
                                          listName: g.name,
                                          codeListMovies: g.id.toString(),
                                        )));
                          },
                        ),
                      ))
                  .toList(),
        ),
      ));

  Widget _buildCompaniesList() => Container(
        height: 80.0,
        child: Padding(
          padding: const EdgeInsets.only(left: 10.0),
          child: ListView(
              scrollDirection: Axis.horizontal,
              children: tvDetails.networks == null
                  ? Container()
                  : tvDetails.networks
                      .map((n) => Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              InkWell(
                                onTap: () {},
                                child: Padding(
                                  padding: const EdgeInsets.only(right: 4.0),
                                  child: Container(
                                      width: 60.0,
                                      height: 60.0,
                                      child: ClipRRect(
                                        borderRadius:
                                            BorderRadius.circular(30.0),
                                        child: Container(
                                            decoration: BoxDecoration(
                                                color: Colors.white),
                                            child: n.logoPath != null
                                                ? Image.network(
                                                    '${Tmdb.baseImageUrl}w300${n.logoPath}',
                                                    fit: BoxFit.contain,
                                                  )
                                                : Container(
                                                    child: Icon(
                                                      Icons.movie,
                                                    ),
                                                  )),
                                      ),
                                      padding: const EdgeInsets.all(
                                          1.5), // borde width
                                      decoration: new BoxDecoration(
                                        color: Colors.orange,
                                        shape: BoxShape.circle,
                                      )),
                                ),
                              ),
                              Container(
                                padding: const EdgeInsets.only(top: 3.0),
                                width: 80.0,
                                child: Column(
                                  children: <Widget>[
                                    Center(
                                      child: Text(
                                        n.name,
                                        style: TextStyle(
                                          fontFamily: 'RalewayMedium',
                                          color: Colors.white,
                                          fontSize: 13.0,
                                        ),
                                        overflow: TextOverflow.ellipsis,
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ))
                      .toList()),
        ),
      );

  Widget _buildMiddleContent() => Card(
      elevation: 5.0,
      color: Color.fromRGBO(13, 13, 15, 1),
      child: Container(
        padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 2.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Divider(),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 30.0,
                  width: 1.0,
                  color: Color.fromRGBO(255, 0, 57, 1),
                ),
                SizedBox(
                  width: 6.0,
                ),
                Text(
                  'Sinopsis',
                  style: TextStyle(
                      fontSize: 20.0,
                      color: Colors.grey[200],
                      fontFamily: 'MontserratMedium'),
                ),
              ],
            ),
            SizedBox(height: 8.0),
            Padding(
                padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                child: tvDetails.overview == ""
                    ? Text(
                        'Sinopsis desconocida',
                        style: TextStyle(
                            color: Colors.grey[300],
                            fontSize: 14.0,
                            fontFamily: 'RalewayMedium'),
                      )
                    : Text(
                        tvDetails.overview,
                        style: TextStyle(
                            color: Colors.grey[300],
                            fontSize: 14.0,
                            fontFamily: 'RalewayMedium'),
                      )),
            SizedBox(
              height: 10.0,
            )
          ],
        ),
      ));

  Widget _buildMovieListItem(Results tvItem) => Container(
        child: Padding(
          padding: const EdgeInsets.only(top: 6.0, bottom: 6.0),
          child: Column(
            children: <Widget>[
              Card(
                elevation: 7.0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0)),
                child: Container(
                    height: 180.0,
                    width: 120.0,
                    child: Stack(children: <Widget>[
                      new ClipRRect(
                        borderRadius: new BorderRadius.circular(3.0),
                        child: InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => TvDetailPage(
                                            tvSerie: tvItem,
                                          )));
                            },
                            child: tvItem.posterPath == null
                                ? Container(
                                    height: 180.0,
                                    width: double.infinity,
                                    color: Color.fromRGBO(35, 47, 52, 1),
                                    child: Icon(
                                      Icons.movie,
                                      color: Colors.grey,
                                    ))
                                : Image.network(
                                    '${Tmdb.baseImageUrl}w185${tvItem.posterPath}',
                                    fit: BoxFit.cover,
                                    width: double.infinity,
                                    height: 180.0,
                                  )),
                      ),
                      Positioned(
                          top: 3.0,
                          right: 3.0,
                          child: Container(
                            height: 15.0,
                            width: 30.0,
                            decoration: BoxDecoration(
                                color: Color.fromRGBO(255, 0, 57, .9),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(7.5))),
                            child: Center(
                              child: Text(
                                "${DateFormat('yyyy').format(DateTime.parse(tvItem.firstAirDate))}",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'RalewayMedium',
                                  fontSize: 11.0,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ))
                    ])),
              ),
              Padding(
                padding: EdgeInsets.only(top: 4.0),
                child: Container(
                  width: 110.0,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Center(
                        child: Text(
                          tvItem.name,
                          style: TextStyle(
                              fontFamily: 'MontserratRegular',
                              fontSize: 12.0,
                              color: Colors.white),
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );

  Widget _buildCardsList(TvSeries tvSerie, String tvListTitle) => Container(
        height: 280.0,
        padding: EdgeInsets.only(top: 6.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 8.0, bottom: 4.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: 30.0,
                        width: 1.0,
                        color: Colors.yellow,
                      ),
                      SizedBox(
                        width: 6.0,
                      ),
                      Text(
                        tvListTitle,
                        style: TextStyle(
                            fontSize: 20.0,
                            fontFamily: 'MontserratMedium',
                            color: Colors.grey[200]),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Flexible(
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: tvSerie == null
                    ? <Widget>[Center(child: CircularProgressIndicator())]
                    : tvSerie.results
                        .map((tvItem) => Padding(
                              padding: EdgeInsets.only(left: 12.0, right: 2.0),
                              child: _buildMovieListItem(tvItem),
                            ))
                        .toList(),
              ),
            )
          ],
        ),
      );

  Widget _builSimilarMoviesList() => Container(
        child:
            ('${Tmdb.baseUrl}${widget.tvSerie.id}/similar?api_key=${Tmdb.apiKey}&language=es&page=1') !=
                    null
                ? _buildCardsList(similarTv, 'Similares')
                : Container(),
      );

  Widget _buildRecommendedMoviesList() => Container(
        child:
            ('${Tmdb.baseUrl}${widget.tvSerie.id}/recommendations?api_key=${Tmdb.apiKey}&language=es&page=1') !=
                    null
                ? _buildCardsList(recommendedTv, 'Recomendadas')
                : Container(),
      );

  Widget _buildViewPage() => ListView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          _buildTopContent(),
          Divider(),
          _buildOptions(),
          Divider(),
          _buildMiddleContent(),
          Divider(),
          _buildGenresList(),
          Divider(),
          _buildCompaniesList(),
          Divider(),
          _buildCastContent(),
          Divider(),
          _buildCrewContent(),
          Divider(),
          _builSimilarMoviesList(),
          SizedBox(
            height: 10.0,
          ),
          _buildRecommendedMoviesList(),
          SizedBox(
            height: 10.0,
          ),
        ],
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(26, 27, 31, 1),
      body: tvDetails == null
          ? Center(
              child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Loader(),
                Text(
                  'Cargando serie..',
                  style: TextStyle(
                      fontFamily: 'RalewayMedium',
                      color: Colors.white,
                      fontSize: 16.0),
                )
              ],
            ))
          : NestedScrollView(
              scrollDirection: Axis.vertical,
              headerSliverBuilder:
                  (BuildContext context, bool innerBoxIsScrolled) {
                return <Widget>[
                  SliverAppBar(
                    backgroundColor: Color.fromRGBO(18, 19, 23, 1),
                    expandedHeight: MediaQuery.of(context).size.height * .35,
                    elevation: 5.0,
                    floating: false,
                    pinned: true,
                    flexibleSpace: FlexibleSpaceBar(
                        centerTitle: true,
                        collapseMode: CollapseMode.pin,
                        title: Container(
                          padding: EdgeInsets.only(top: 3.0),
                          width: MediaQuery.of(context).size.width * .65,
                          child: Text(
                            widget.tvSerie.name,
                            style: TextStyle(
                              fontSize: 16.0,
                              color: Colors.white,
                              fontFamily: 'MontserratMedium',
                            ),
                            overflow: TextOverflow.ellipsis,
                            softWrap: true,
                            maxLines: 2,
                          ),
                        ),
                        background: widget.tvSerie.backdropPath == null
                            ? Container(
                                width: double.infinity,
                                color: Color.fromRGBO(35, 47, 52, 1),
                                child: Icon(
                                  Icons.movie,
                                  size: 60.0,
                                  color: Colors.grey,
                                ))
                            : Image.network(
                                '${Tmdb.baseImageUrl}w780${widget.tvSerie.backdropPath}',
                                fit: BoxFit.cover,
                              )),
                  ),
                ];
              },
              body: TabBarView(
                children: <Widget>[
                  _buildViewPage(),
                  new SeasonsPage(idShow: tvDetails.id),
                ],
                controller: tabController,
              ),
            ),
      bottomNavigationBar: new TabBar(
        unselectedLabelColor: Color.fromRGBO(69, 73, 79, 1),
        indicatorColor: Color.fromRGBO(255, 0, 57, .8),
        labelColor: Color.fromRGBO(255, 0, 57, .8),
        labelStyle: TextStyle(fontFamily: 'RalewayMedium', fontSize: 17.0),
        labelPadding: EdgeInsets.all(0.0),
        controller: tabController,
        tabs: <Widget>[
          new Tab(
            text: 'INFO',
          ),
          new Tab(
            text: 'TEMPORADAS',
          ),
        ],
      ),
    );
  }
}
