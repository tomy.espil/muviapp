import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:muvi_app/helpers/loader.dart';
import 'package:muvi_app/models/imagesModel.dart';
import 'package:muvi_app/models/movieDetailModel.dart';
import 'package:muvi_app/models/movieTvCredits.dart';
import 'package:muvi_app/models/movieModel.dart';
import 'package:muvi_app/models/tmdb.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:muvi_app/models/videosModel.dart';
import 'package:muvi_app/pages/extendListPage.dart';
import 'package:muvi_app/pages/images_page.dart';
import 'package:muvi_app/pages/peopleDetails_page.dart';
import 'package:muvi_app/pages/videos_page.dart';
import 'package:muvi_app/pages/webSite_page.dart';
import 'package:muvi_app/widgets/list_Items.dart';
class MovieDetailPage extends StatefulWidget {
  final Movies movie;

  MovieDetailPage({this.movie});

  @override
  _MovieDetailsPage createState() => new _MovieDetailsPage();
}

class _MovieDetailsPage extends State<MovieDetailPage> {
  String movieDetailUrl;
  String movieCreditsUrl;
  String similarMoviesUrl;
  String recommendedMoviesUrl;
  String imagesUrl;
  String urlVideosEsp;
  String urlVideosIng;
  MovieDetailModel movieDetails;
  MovieTvCredits movieTvCredits;
  ImagesModel imagesModel;
  VideosModel videosModel;
  Movie similarMovies;
  Movie recommendedMovies;
  final oCcy = new NumberFormat("#,###", "es_AR");
  bool isLoading;

  int heroTag = 1;

  final dt = DateFormat('yyyy');

  @override
  void initState() {
    super.initState();
    movieDetailUrl =
        '${Tmdb.baseUrl}${widget.movie.id}?api_key=${Tmdb.apiKey}&language=es';
    _fetchMovieDetails();
    movieCreditsUrl =
        '${Tmdb.baseUrl}${widget.movie.id}/credits?api_key=${Tmdb.apiKey}';
    _fetchMovieCredits();
    imagesUrl =
        '${Tmdb.baseUrl}${widget.movie.id}/images?api_key=${Tmdb.apiKey}&language=es-US&include_image_language=es%2Cnull';
    _fetchImages();
    urlVideosEsp =
        '${Tmdb.baseUrl}${widget.movie.id}/videos?api_key=${Tmdb.apiKey}&language=es';
    _fetchVideosEsp();
    urlVideosIng =
        '${Tmdb.baseUrl}${widget.movie.id}/videos?api_key=${Tmdb.apiKey}&language=en';
    _fetchVideosIng();
    similarMoviesUrl =
        '${Tmdb.baseUrl}${widget.movie.id}/similar?api_key=${Tmdb.apiKey}&language=es&page=1';
    _fetchSimilarMovies();
    recommendedMoviesUrl =
        '${Tmdb.baseUrl}${widget.movie.id}/recommendations?api_key=${Tmdb.apiKey}&language=es&page=1';
    _fetchRecommendedMovies();
    isLoading = false;
  }

  void _fetchMovieDetails() async {
    var response = await http.get(movieDetailUrl);
    var decodedJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      movieDetails = MovieDetailModel.fromJson(decodedJson);
    });
  }

  void _fetchMovieCredits() async {
    var response = await http.get(movieCreditsUrl);
    var decodedJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      movieTvCredits = MovieTvCredits.fromJson(decodedJson);
    });
  }

  void _fetchImages() async {
    var response = await http.get(imagesUrl);
    var decodedJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      imagesModel = ImagesModel.fromJson(decodedJson);
    });
  }

  void _fetchVideosEsp() async {
    var response = await http.get(urlVideosEsp);
    var decodedJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      videosModel = VideosModel.fromJson(decodedJson);
    });
  }

  void _fetchVideosIng() async {
    var response = await http.get(urlVideosIng);
    var decodedJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      videosModel = VideosModel.fromJson(decodedJson);
    });
  }

  void _fetchSimilarMovies() async {
    var response = await http.get(similarMoviesUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      similarMovies = Movie.fromJson(decodeJson);
    });
  }

  void _fetchRecommendedMovies() async {
    var response = await http.get(recommendedMoviesUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      recommendedMovies = Movie.fromJson(decodeJson);
    });
  }

  String _getMovieDuration(int runtime) {
    if (runtime == null) return 'Sin datos';
    double movieHours = runtime / 60;
    int movieMinutes = ((movieHours - movieHours.floor()) * 60).round();
    return "${movieHours.floor()}h ${movieMinutes}min";
  }

  String _getMoviePopularity(double popularity) {
    if (popularity == null) return 'Sin datos';
    return '$popularity';
  }

  Widget _buildCastContent() => Container(
        height: 220.0,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
            Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 30.0,
                  width: 1.0,
                  color: Color.fromRGBO(255, 0, 57, 1),
                ),
                SizedBox(
                  width: 6.0,
                ),
                Text('Reparto',
                    style: TextStyle(
                        fontSize: 20.0,
                        fontFamily: 'MontserratMedium',
                        color: Colors.grey[200])),
              ],
            ),
          ),
          Flexible(
            child: ListView(
              padding: EdgeInsets.only(left: 12.0, bottom: 4.0),
              scrollDirection: Axis.horizontal,
              children: movieTvCredits == null || isLoading == true
                  ? <Widget>[
                      Center(
                          child: CircularProgressIndicator(
                        backgroundColor: Colors.green,
                      ))
                    ]
                  : movieTvCredits.cast
                      .map(
                        (c) => Padding(
                              padding: EdgeInsets.only(right: 10.0),
                              child: Column(
                                children: <Widget>[
                                  Stack(children: <Widget>[
                                    Container(
                                      child: InkWell(
                                          onTap: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        PeoplePage(
                                                            person: c.id)));
                                          },
                                          child: Container(
                                            height: 150.0,
                                            width: 100.0,
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(7.0),
                                              child: c.profilePath != null
                                                  ? Image.network(
                                                      '${Tmdb.baseImageUrl}h632${c.profilePath}',
                                                      fit: BoxFit.cover,
                                                    )
                                                  : Image.asset(
                                                      'assets/nobody.jpg',
                                                      fit: BoxFit.cover,
                                                    ),
                                            ),
                                          )),
                                    ),
                                    Positioned(
                                        bottom: 5.0,
                                        left: 2.0,
                                        child: Container(
                                          width: 90.0,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: <Widget>[
                                              Text(
                                                c.name,
                                                style: TextStyle(
                                                  fontFamily:
                                                      'MontserratMedium',
                                                  color: Colors.white,
                                                  fontSize: 12.0,
                                                ),
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 2,
                                                textAlign: TextAlign.center,
                                              ),
                                            ],
                                          ),
                                        ))
                                  ]),
                                  SizedBox(
                                    height: 4.0,
                                  ),
                                  Container(
                                    width: 90.0,
                                    child: Text(
                                      '"${c.character}"',
                                      style: TextStyle(
                                          fontSize: 11.0,
                                          fontFamily: 'MontserratRegular',
                                          color: Colors.white70,
                                          fontStyle: FontStyle.italic),
                                      overflow: TextOverflow.ellipsis,
                                      textAlign: TextAlign.center,
                                      maxLines: 2,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                      )
                      .toList(),
            ),
          )
        ]),
      );

  Widget _buildCrewContent() => Container(
        height: 220.0,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
            Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 30.0,
                  width: 1.0,
                  color: Color.fromRGBO(255, 0, 57, 1),
                ),
                SizedBox(
                  width: 6.0,
                ),
                Text('Produccion',
                    style: TextStyle(
                        fontSize: 20.0,
                        fontFamily: 'MontserratMedium',
                        color: Colors.grey[200])),
              ],
            ),
          ),
          Flexible(
            child: ListView(
              padding: EdgeInsets.only(left: 12.0, bottom: 4.0),
              scrollDirection: Axis.horizontal,
              children: movieTvCredits == null || isLoading == true
                  ? <Widget>[
                      Center(
                          child: CircularProgressIndicator(
                        backgroundColor: Colors.green,
                      ))
                    ]
                  : movieTvCredits.crew
                      .map(
                        (c) => Padding(
                              padding: EdgeInsets.only(right: 10.0),
                              child: Column(
                                children: <Widget>[
                                  Stack(children: <Widget>[
                                    Container(
                                      child: InkWell(
                                          onTap: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        PeoplePage(
                                                            person: c.id)));
                                          },
                                          child: Container(
                                            height: 150.0,
                                            width: 100.0,
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(7.0),
                                              child: c.profilePath != null
                                                  ? Image.network(
                                                      '${Tmdb.baseImageUrl}h632${c.profilePath}',
                                                      fit: BoxFit.cover,
                                                    )
                                                  : Image.asset(
                                                      'assets/nobody.jpg',
                                                      fit: BoxFit.cover,
                                                    ),
                                            ),
                                          )),
                                    ),
                                    Positioned(
                                        bottom: 5.0,
                                        left: 2.0,
                                        child: Container(
                                          width: 90.0,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: <Widget>[
                                              Text(
                                                c.name,
                                                style: TextStyle(
                                                  fontFamily:
                                                      'MontserratMedium',
                                                  color: Colors.white,
                                                  fontSize: 12.0,
                                                ),
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 2,
                                                textAlign: TextAlign.center,
                                              ),
                                            ],
                                          ),
                                        ))
                                  ]),
                                  SizedBox(
                                    height: 4.0,
                                  ),
                                  Container(
                                    width: 90.0,
                                    child: Text(
                                      '"${c.job}"',
                                      style: TextStyle(
                                          fontSize: 11.0,
                                          fontFamily: 'MontserratRegular',
                                          color: Colors.white70,
                                          fontStyle: FontStyle.italic),
                                      overflow: TextOverflow.ellipsis,
                                      textAlign: TextAlign.center,
                                      maxLines: 2,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                      )
                      .toList(),
            ),
          )
        ]),
      );

  Widget _buildTopContent() =>
      Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
        Container(
            child: movieDetails.tagline == null || movieDetails.tagline == ""
                ? Container(
                    height: 0,
                    width: 0,
                  )
                : Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 14.0, right: 14.0, bottom: 5.0),
                        child: Text(movieDetails.tagline,
                            style: TextStyle(
                              fontFamily: 'MontserratRegular',
                              fontSize: 13.0,
                              color: Colors.white,
                              fontStyle: FontStyle.italic,
                            ),
                            textScaleFactor: 1.0,
                            textAlign: TextAlign.center,),
                      ),
                      SizedBox(height: 12.0),
                    ],
                  )),
        Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 7.0, right: 7.0),
                child: Container(
                  height: 180.0,
                  width: 120.0,
                  alignment: FractionalOffset.center,
                  child: ClipRRect(
                      borderRadius: new BorderRadius.circular(4.0),
                      child: widget.movie.posterPath == null
                          ? Container(
                              height: 180.0,
                              width: 120.0,
                              color: Color.fromRGBO(35, 47, 52, 1),
                              child: Icon(
                                Icons.movie,
                                color: Colors.grey,
                              ))
                          : Image.network(
                              '${Tmdb.baseImageUrl}w780${widget.movie.posterPath}',
                              fit: BoxFit.cover,
                              height: 180.0,
                              width: 120.0)),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 5.0),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Icon(
                            Icons.access_time,
                            color: Colors.white70,
                            size: 20.0,
                          ),
                          SizedBox(
                            width: 7.0,
                          ),
                          Text(
                              movieDetails == null || movieDetails.runtime == 0
                                  ? ""
                                  : _getMovieDuration(movieDetails.runtime),
                              style: TextStyle(
                                fontSize: 14.0,
                                fontFamily: 'MontserratMedium',
                                color: Colors.white,
                              )),
                        ],
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Icon(
                            Icons.date_range,
                            color: Colors.white70,
                            size: 20.0,
                          ),
                          SizedBox(
                            width: 7.0,
                          ),
                          Container(
                              child: movieDetails == null ||
                                      movieDetails.releaseDate == ""
                                  ? Text("Sin informacion de estreno",
                                      style: TextStyle(
                                        fontSize: 14.0,
                                        fontFamily: 'MontserratMedium',
                                        color: Colors.white,
                                      ))
                                  : Text(
                                      "${dt.format(DateTime.tryParse(movieDetails.releaseDate))}",
                                      style: TextStyle(
                                        fontSize: 14.0,
                                        fontFamily: 'MontserratMedium',
                                        color: Colors.white,
                                      ))),
                        ],
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Row(
                        children: <Widget>[
                          Icon(
                            Icons.star,
                            color: Colors.white70,
                            size: 20.0,
                          ),
                          SizedBox(
                            width: 7.0,
                          ),
                          Text(
                            movieDetails == null ||
                                    movieDetails.voteAverage == 0.0
                                ? '-'
                                : '${movieDetails.voteAverage}/10 (${oCcy.format(movieDetails.voteCount)})',
                            style: TextStyle(
                                fontSize: 14.0,
                                fontFamily: 'MontserratMedium',
                                color: Colors.white),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Icon(
                            Icons.people,
                            color: Colors.white70,
                            size: 20.0,
                          ),
                          SizedBox(
                            width: 7.0,
                          ),
                          Text(
                            movieDetails == null || movieDetails.popularity == 0
                                ? '-'
                                : _getMoviePopularity(movieDetails.popularity),
                            style: TextStyle(
                                fontSize: 14.0,
                                fontFamily: 'MontserratMedium',
                                color: Colors.white),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Row(
                        children: <Widget>[
                          Icon(
                            Icons.monetization_on,
                            color: Colors.white70,
                            size: 20.0,
                          ),
                          SizedBox(
                            width: 7.0,
                          ),
                          Text(
                            movieDetails == null || movieDetails.revenue == 0
                                ? '-'
                                : '\$ ${oCcy.format(movieDetails.revenue)}',
                            style: TextStyle(
                                fontSize: 14.0,
                                fontFamily: 'MontserratMedium',
                                color: Colors.white),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Row(
                        children: <Widget>[
                          Icon(
                            Icons.language,
                            color: Colors.white70,
                            size: 20.0,
                          ),
                          SizedBox(
                            width: 7.0,
                          ),
                          Container(
                              width: MediaQuery.of(context).size.width * .5,
                              child: movieDetails == null ||
                                      movieDetails.productionCountries == []
                                  ? Text('-')
                                  : Text(
                                      movieDetails.productionCountries
                                          .map((c) => c.name)
                                          .toString(),
                                      style: TextStyle(
                                          fontSize: 14.0,
                                          fontFamily: 'MontserratMedium',
                                          color: Colors.white),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                    ))
                        ],
                      ),
                    ]),
              ),
            ]),
        SizedBox(
          height: 10.0,
        ),
        Container(
            width: MediaQuery.of(context).size.width * .9,
            height: 35.0,
            child: movieDetails.homepage == "" || movieDetails.homepage == null
                ? MaterialButton(
                    color: Colors.grey,
                    minWidth: MediaQuery.of(context).size.width * .9,
                    child: Text(
                      'SITIO WEB NO DISPONIBLE',
                      style: TextStyle(
                          fontFamily: 'RalewayMedium',
                          fontSize: 14.0,
                          color: Colors.white),
                      textScaleFactor: 1.0,
                    ),
                    onPressed: () {},
                  )
                : MaterialButton(
                    color: Color.fromRGBO(255, 0, 57, 1),
                    minWidth: MediaQuery.of(context).size.width * .9,
                    child: Text(
                      'SITIO WEB',
                      style: TextStyle(
                          fontFamily: 'RalewayMedium',
                          fontSize: 14.0,
                          color: Colors.white),
                      textScaleFactor: 1.0,
                    ),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => WebSitePage(
                                    name: movieDetails.title,
                                    urlWeb: movieDetails.homepage,
                                  )));
                    },
                  ))
      ]);

  Widget _buildOptions() => Padding(
        padding: EdgeInsets.only(right: 5.0, left: 5.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            MaterialButton(
              height: 50.0,
              minWidth: MediaQuery.of(context).size.width * .32,
              color: Color.fromRGBO(13, 13, 15, 1),
              child: Column(
                children: <Widget>[
                  Icon(
                    Icons.favorite_border,
                    color: Colors.white,
                  ),
                  Text(
                    'Favorita',
                    style: TextStyle(
                        fontFamily: 'RalewayRegular', color: Colors.white),
                  )
                ],
              ),
              onPressed: () {},
            ),
            MaterialButton(
              height: 50.0,
              minWidth: MediaQuery.of(context).size.width * .32,
              color: Color.fromRGBO(13, 13, 15, 1),
              child: Column(
                children: <Widget>[
                  Icon(
                    CupertinoIcons.add,
                    color: Colors.white,
                  ),
                  Text(
                    'Para ver',
                    style: TextStyle(
                        fontFamily: 'RalewayRegular', color: Colors.white),
                  )
                ],
              ),
              onPressed: () {},
            ),
            MaterialButton(
              height: 50.0,
              minWidth: MediaQuery.of(context).size.width * .32,
              color: Color.fromRGBO(13, 13, 15, 1),
              child: Column(
                children: <Widget>[
                  Icon(
                    CupertinoIcons.check_mark_circled,
                    color: Colors.white,
                  ),
                  Text(
                    'Vista',
                    style: TextStyle(
                        fontFamily: 'RalewayRegular', color: Colors.white),
                  )
                ],
              ),
              onPressed: () {},
            )
          ],
        ),
      );

  Widget _buildMiddleContent() => Card(
      elevation: 5.0,
      color: Color.fromRGBO(13, 13, 15, 1),
      child: Container(
        padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 2.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Divider(),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 30.0,
                  width: 1.0,
                  color: Color.fromRGBO(255, 0, 57, 1),
                ),
                SizedBox(
                  width: 6.0,
                ),
                Text(
                  'Sinopsis',
                  style: TextStyle(
                      fontSize: 20.0,
                      color: Colors.grey[200],
                      fontFamily: 'MontserratMedium'),
                ),
              ],
            ),
            SizedBox(height: 8.0),
            Padding(
                padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                child: movieDetails.overview == ""
                    ? Text(
                        'Sinopsis desconocida',
                        style: TextStyle(
                            color: Colors.grey[300],
                            fontSize: 14.0,
                            fontFamily: 'RalewayMedium'),
                        textScaleFactor: 1.0,
                      )
                    : Text(
                        movieDetails.overview,
                        style: TextStyle(
                          color: Colors.white70,
                          fontSize: 12.0,
                          fontFamily: 'RalewayMedium',
                        ),
                        textScaleFactor: 1.0,
                        softWrap: true,
                      )),
            SizedBox(
              height: 10.0,
            )
          ],
        ),
      ));

  Widget _buildGenresList() => Container(
      height: 24.0,
      child: Padding(
        padding: const EdgeInsets.only(left: 14.0),
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: movieDetails == null || movieDetails.genres == [null]
              ? <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 14.0),
                    child: Text('Sin datos de generos',
                        style: TextStyle(
                            color: Colors.white, fontFamily: 'RalewayMedium')),
                  ),
                ]
              : movieDetails.genres
                  .map((g) => Padding(
                        padding: const EdgeInsets.only(right: 6.0),
                        child: FilterChip(
                          backgroundColor: Color.fromRGBO(255, 0, 57, 1),
                          labelStyle: TextStyle(fontSize: 11.0),
                          label: Padding(
                            padding: const EdgeInsets.only(bottom: 8.0),
                            child: Text(g.name,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'RalewayBold',
                                    fontSize: 13.0)),
                          ),
                          onSelected: (b) {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ExtendListPage(
                                          listName: g.name,
                                          codeListMovies: g.id.toString(),
                                        )));
                          },
                        ),
                      ))
                  .toList(),
        ),
      ));

  Widget _buildCompaniesList() => Container(
        height: 115.0,
        child: Padding(
          padding: const EdgeInsets.only(left: 10.0),
          child: ListView(
              scrollDirection: Axis.horizontal,
              children: movieDetails == null ||
                      movieDetails.productionCompanies == []
                  ? Container(
                      height: 60.0,
                      child: Text('Sin datos de compañias',
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'RalewayMedium')))
                  : movieDetails.productionCompanies
                      .map((p) => Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(right: 8.0),
                                child: Container(
                                  width: 100.0,
                                  height: 80.0,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(7.0),
                                    child: Container(
                                        padding: EdgeInsets.all(2.0),
                                        decoration:
                                            BoxDecoration(color: Colors.white),
                                        child: p.logoPath != null
                                            ? Image.network(
                                                '${Tmdb.baseImageUrl}w300${p.logoPath}',
                                                fit: BoxFit.contain,
                                              )
                                            : Container(
                                                child: Icon(
                                                  Icons.movie,
                                                ),
                                              )),
                                  ),
                                ),
                              ),
                              Container(
                                padding: const EdgeInsets.only(top: 3.0),
                                width: 80.0,
                                child: Column(
                                  children: <Widget>[
                                    Center(
                                      child: Text(
                                        p.name,
                                        style: TextStyle(
                                          fontFamily: 'MontserratLight',
                                          color: Colors.white,
                                          fontSize: 12.0,
                                        ),
                                        overflow: TextOverflow.ellipsis,
                                        textAlign: TextAlign.center,
                                        maxLines: 2,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ))
                      .toList()),
        ),
      );

  Widget _buildImagesList() => Container(
      height: 250.0,
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 30.0,
                  width: 1.0,
                  color: Color.fromRGBO(255, 0, 57, 1),
                ),
                SizedBox(
                  width: 6.0,
                ),
                Text('Imagenes',
                    style: TextStyle(
                        fontSize: 20.0,
                        fontFamily: 'MontserratMedium',
                        color: Colors.grey[200])),
              ],
            ),
          ),
          Flexible(
            child: ListView(
              padding: EdgeInsets.only(left: 12.0, bottom: 4.0, right: 5.0),
              scrollDirection: Axis.horizontal,
              children: imagesModel == null
                  ? <Widget>[Center(child: CircularProgressIndicator())]
                  : imagesModel.backdrops
                      .map((g) => Padding(
                          padding: const EdgeInsets.only(
                              left: 4.0, right: 6.0, bottom: 4.0, top: 4.0),
                          child: Hero(
                            tag: g.filePath,
                            child: GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ImagesPage(
                                              filePath: g.filePath,
                                            )));
                              },
                              child: Container(
                                width: 220.0,
                                height: 150.0,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(5.0),
                                  child: Image.network(
                                    '${Tmdb.baseImageUrl}w780${g.filePath}',
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                            ),
                          )))
                      .toList(),
            ),
          ),
        ],
      ));

  
  Widget _buildCardsList(Movie movie) => Container(
        height: 240.0,
        child: ListView(
          padding: EdgeInsets.only(top: 4.0, left: 12.0, bottom: 4.0),
          scrollDirection: Axis.horizontal,
          children: movie == null || isLoading == true
              ? <Widget>[
                  Center(
                      child: CircularProgressIndicator(
                    backgroundColor: Colors.green,
                  ))
                ]
              : movie.movies
                  .map(
                    (movieItem) => ListItems(item: movieItem, loading: isLoading,),
                  )
                  .toList(),
        ),
      );

  Widget _builSimilarMoviesList() => Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 8.0, bottom: 4.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: 30.0,
                        width: 1.0,
                        color: Colors.yellow,
                      ),
                      SizedBox(
                        width: 6.0,
                      ),
                      Text(
                        'Pueden interesarte',
                        style: TextStyle(
                            fontSize: 17.0,
                            fontFamily: 'MontserratMedium',
                            color: Colors.grey[200]),
                        textScaleFactor: 1.0,
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Container(
                child: similarMovies == null || similarMovies.totalResults == 0
                    ? Padding(
                        padding: EdgeInsets.only(left: 14.0, top: 6.0),
                        child: Text(
                          'No hay peliculas para mostrar',
                          style: TextStyle(
                              color: Colors.grey,
                              fontSize: 14.0,
                              fontFamily: 'RalewayMedium'),
                        ))
                    : _buildCardsList(similarMovies))
          ],
        ),
      );

  Widget _buildRecommendedMoviesList() => Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 8.0, bottom: 4.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: 30.0,
                        width: 1.0,
                        color: Colors.yellow,
                      ),
                      SizedBox(
                        width: 6.0,
                      ),
                      Text(
                        'Otros usuarios tambien buscaron',
                        style: TextStyle(
                            fontSize: 17.0,
                            fontFamily: 'MontserratMedium',
                            color: Colors.grey[200]),
                            textScaleFactor: 1.0,
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Container(
                child: recommendedMovies == null ||
                        recommendedMovies.totalResults == 0
                    ? Padding(
                        padding: EdgeInsets.only(left: 14.0, top: 6.0),
                        child: Text(
                          'No hay peliculas para mostrar',
                          style: TextStyle(
                              color: Colors.grey,
                              fontSize: 14.0,
                              fontFamily: 'RalewayMedium'),
                        ))
                    : _buildCardsList(recommendedMovies))
          ],
        ),
      );

  Widget _buildTopPart() => Container(
        height: MediaQuery.of(context).size.height * .5,
        child: Stack(
          children: <Widget>[
            ClipPath(
              clipper: Mclipper(),
              child: Container(
                height: MediaQuery.of(context).size.height * .45,
                decoration: BoxDecoration(color: Colors.white, boxShadow: [
                  BoxShadow(
                      color: Colors.black12,
                      offset: Offset(0.0, 10.0),
                      blurRadius: 10.0)
                ]),
                child: Stack(
                  children: <Widget>[
                    Container(
                        child: widget.movie.backdropPath == null
                            ? Container(
                                width: double.infinity,
                                height:
                                    MediaQuery.of(context).size.height * .45,
                                color: Color.fromRGBO(35, 47, 52, 1),
                                child: Icon(
                                  Icons.movie,
                                  size: 60.0,
                                  color: Colors.grey,
                                ))
                            : Image.network(
                                '${Tmdb.baseImageUrl}w1280${widget.movie.backdropPath}',
                                fit: BoxFit.cover,
                                width: double.infinity,
                                height:
                                    MediaQuery.of(context).size.height * .45,
                              )),
                    Container(
                      height: double.infinity,
                      width: double.infinity,
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              colors: [
                            const Color(0x00000000),
                            const Color(0xD9333333)
                          ],
                              stops: [
                            0.0,
                            0.9
                          ],
                              begin: FractionalOffset(0.0, 0.0),
                              end: FractionalOffset(0.0, 1.0))),
                      child: Padding(
                        padding: EdgeInsets.only(top: 100.0, left: 75.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              movieDetails.title,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 28.0,
                                  fontFamily: "MontserratBold"),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Positioned(
              top: 290.0,
              right: -25.0,
              child: FractionalTranslation(
                translation: Offset(0.0, -0.5),
                child: Row(
                  children: <Widget>[
                    ClipRRect(
                        borderRadius: BorderRadius.circular(30.0),
                        child: videosModel == null
                            ? RaisedButton(
                                onPressed: () {},
                                color: Colors.grey,
                                padding: EdgeInsets.symmetric(
                                    vertical: 12.0, horizontal: 50.0),
                                child: Row(
                                  children: <Widget>[
                                    Text(
                                      "VIDEOS",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 13.0,
                                          fontFamily: "MontserratMedium"),
                                          textScaleFactor: 1.0,
                                    ),
                                    SizedBox(
                                      width: 10.0,
                                    ),
                                    RotatedBox(
                                      quarterTurns: 2,
                                      child: Icon(Icons.arrow_back,
                                          size: 25.0, color: Colors.white),
                                    )
                                  ],
                                ),
                              )
                            : RaisedButton(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => VideosPage(
                                              movieId: movieDetails.id,
                                              name: movieDetails.title)));
                                },
                                color: Color.fromRGBO(255, 0, 57, 1),
                                padding: EdgeInsets.symmetric(
                                    vertical: 12.0, horizontal: 50.0),
                                child: Row(
                                  children: <Widget>[
                                    Text(
                                      "VIDEOS",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 15.0,
                                          fontFamily: "MontserratMedium"),
                                    ),
                                    SizedBox(
                                      width: 10.0,
                                    ),
                                    RotatedBox(
                                      quarterTurns: 2,
                                      child: Icon(Icons.arrow_back,
                                          size: 25.0, color: Colors.white),
                                    )
                                  ],
                                ),
                              ))
                  ],
                ),
              ),
            ),
            Positioned(
              top: 10.0,
              left: 10.0,
              child: BackButton(
                color: Colors.white,
              ),
            )
          ],
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color.fromRGBO(26, 27, 31, 1),
        body: movieDetails == null
            ? Center(
                child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Loader(),
                  Text(
                    'Cargando pelicula..',
                    style: TextStyle(
                        fontFamily: 'RalewayMedium',
                        color: Colors.white,
                        fontSize: 16.0),
                  )
                ],
              ))
            : ListView(
                children: <Widget>[
                  _buildTopPart(),
                  SizedBox(
                    height: 12.0,
                  ),
                  _buildTopContent(),
                  SizedBox(
                    height: 15.0,
                  ),
                  _buildOptions(),
                  SizedBox(
                    height: 15.0,
                  ),
                  _buildMiddleContent(),
                  SizedBox(
                    height: 25.0,
                  ),
                  _buildGenresList(),
                  SizedBox(
                    height: 30.0,
                  ),
                  _buildCompaniesList(),
                  SizedBox(
                    height: 30.0,
                  ),
                  _buildCastContent(),
                  SizedBox(
                    height: 30.0,
                  ),
                  _buildCrewContent(),
                  SizedBox(
                    height: 30.0,
                  ),
                  _buildImagesList(),
                  SizedBox(
                    height: 30.0,
                  ),
                  _builSimilarMoviesList(),
                  SizedBox(
                    height: 30.0,
                  ),
                  _buildRecommendedMoviesList(),
                  SizedBox(
                    height: 10.0,
                  ),
                ],
                /* ), */
              ));
  }
}

class Mclipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();
    path.lineTo(0.0, size.height - 100.0);

    var controlpoint = Offset(35.0, size.height);
    var endpoint = Offset(size.width / 2, size.height);

    path.quadraticBezierTo(
        controlpoint.dx, controlpoint.dy, endpoint.dx, endpoint.dy);

    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0.0);

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
