import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'dart:convert';

import 'package:muvi_app/models/tmdb.dart';
import 'package:muvi_app/models/tvSeriesModel.dart';
import 'package:muvi_app/pages/tvAiringToday.dart';
import 'package:muvi_app/pages/tvDetails_page.dart';
import 'package:muvi_app/pages/tvOnTheAir.dart';
import 'package:muvi_app/pages/tvPopular.dart';
import 'package:muvi_app/pages/tvTopRated.dart';
import 'package:muvi_app/widgets/card_ListTv.dart';

class TvSeriesPage extends StatefulWidget {
  @override
  _TvSeriesPageState createState() => new _TvSeriesPageState();
}

class _TvSeriesPageState extends State<TvSeriesPage> {
  TvSeries trendingTvWeek;
  TvSeries action;
  TvSeries adventure;
  TvSeries animation;
  TvSeries comedia;
  TvSeries crimen;
  TvSeries documental;
  TvSeries drama;
  TvSeries familia;
  TvSeries fantasia;
  TvSeries historia;
  TvSeries terror;
  TvSeries musica;
  TvSeries misterio;
  TvSeries romance;
  TvSeries cienciaFiccion;
  TvSeries suspenso;
  TvSeries belicas;
  TvSeries western;
  TvSeries de2019;
  TvSeries de2018;
  TvSeries de2017;
  TvSeries de2016;
  TvSeries de2015;

  String trendingTvWeekUrl;
  String actionUrl;
  String adventureUrl;
  String animationUrl;
  String comediaUrl;
  String crimenUrl;
  String documentalUrl;
  String dramaUrl;
  String familiaUrl;
  String fantasiaUrl;
  String historiaUrl;
  String terrorUrl;
  String musicaUrl;
  String misterioUrl;
  String romanceUrl;
  String cienciaFiccionUrl;
  String suspensoUrl;
  String belicasUrl;
  String westernUrl;
  String de2019Url;
  String de2018Url;
  String de2017Url;
  String de2016Url;
  String de2015Url;

  final dt = DateFormat('yyyy');
  TabController tabController;
  bool loadingPage = true;

  @override
  void initState() {
    super.initState();
    trendingTvWeekUrl =
        '${Tmdb.urlSimple}trending/tv/day?api_key=${Tmdb.apiKey}';
    actionUrl =
        '${Tmdb.urlSimple}discover/tv?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&timezone=America%2FArgentina%2FBuenos_Aires&with_genres=${Tmdb.actionCode}&include_null_first_air_dates=false';
    adventureUrl =
        '${Tmdb.urlSimple}discover/tv?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&timezone=America%2FArgentina%2FBuenos_Aires&with_genres=${Tmdb.adventureCode}&include_null_first_air_dates=false';
    animationUrl =
        '${Tmdb.urlSimple}discover/tv?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&timezone=America%2FArgentina%2FBuenos_Aires&with_genres=${Tmdb.animacionCode}&include_null_first_air_dates=false';
    comediaUrl =
        '${Tmdb.urlSimple}discover/tv?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&timezone=America%2FArgentina%2FBuenos_Aires&with_genres=${Tmdb.comediaCode}&include_null_first_air_dates=false';
    crimenUrl =
        '${Tmdb.urlSimple}discover/tv?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&timezone=America%2FArgentina%2FBuenos_Aires&with_genres=${Tmdb.crimenCode}&include_null_first_air_dates=false';
    documentalUrl =
        '${Tmdb.urlSimple}discover/tv?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&timezone=America%2FArgentina%2FBuenos_Aires&with_genres=${Tmdb.documentalCode}&include_null_first_air_dates=false';
    dramaUrl =
        '${Tmdb.urlSimple}discover/tv?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&timezone=America%2FArgentina%2FBuenos_Aires&with_genres=${Tmdb.dramaCode}&include_null_first_air_dates=false';
    familiaUrl =
        '${Tmdb.urlSimple}discover/tv?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&timezone=America%2FArgentina%2FBuenos_Aires&with_genres=${Tmdb.familiaCode}&include_null_first_air_dates=false';
    fantasiaUrl =
        '${Tmdb.urlSimple}discover/tv?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&timezone=America%2FArgentina%2FBuenos_Aires&with_genres=${Tmdb.fantasiaCode}&include_null_first_air_dates=false';
    historiaUrl =
        '${Tmdb.urlSimple}discover/tv?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&timezone=America%2FArgentina%2FBuenos_Aires&with_genres=${Tmdb.historiaCode}&include_null_first_air_dates=false';
    terrorUrl =
        '${Tmdb.urlSimple}discover/tv?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&timezone=America%2FArgentina%2FBuenos_Aires&with_genres=${Tmdb.terrorCode}&include_null_first_air_dates=false';
    musicaUrl =
        '${Tmdb.urlSimple}discover/tv?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&timezone=America%2FArgentina%2FBuenos_Aires&with_genres=${Tmdb.musicaCode}&include_null_first_air_dates=false';
    misterioUrl =
        '${Tmdb.urlSimple}discover/tv?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&timezone=America%2FArgentina%2FBuenos_Aires&with_genres=${Tmdb.misterioCode}&include_null_first_air_dates=false';
    romanceUrl =
        '${Tmdb.urlSimple}discover/tv?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&timezone=America%2FArgentina%2FBuenos_Aires&with_genres=${Tmdb.romanceCode}&include_null_first_air_dates=false';
    cienciaFiccionUrl =
        '${Tmdb.urlSimple}discover/tv?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&timezone=America%2FArgentina%2FBuenos_Aires&with_genres=${Tmdb.cienciaFiccionCode}&include_null_first_air_dates=false';
    suspensoUrl =
        '${Tmdb.urlSimple}discover/tv?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&timezone=America%2FArgentina%2FBuenos_Aires&with_genres=${Tmdb.suspensoCode}&include_null_first_air_dates=false';
    belicasUrl =
        '${Tmdb.urlSimple}discover/tv?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&timezone=America%2FArgentina%2FBuenos_Aires&with_genres=${Tmdb.belicaCode}&include_null_first_air_dates=false';
    westernUrl =
        '${Tmdb.urlSimple}discover/tv?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&timezone=America%2FArgentina%2FBuenos_Aires&with_genres=${Tmdb.westernCode}&include_null_first_air_dates=false';
    de2019Url =
        '${Tmdb.urlSimple}discover/tv?api_key=${Tmdb.apiKey}&language=es&sort_by=release_date.asc&include_adult=false&page=1&timezone=America%2FArgentina%2FBuenos_Aires&first_air_date_year=2019&include_null_first_air_dates=false';
    de2018Url =
        '${Tmdb.urlSimple}discover/tv?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&timezone=America%2FArgentina%2FBuenos_Aires&first_air_date_year=2018&include_null_first_air_dates=false';
    de2017Url =
        '${Tmdb.urlSimple}discover/tv?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&timezone=America%2FArgentina%2FBuenos_Aires&first_air_date_year=2017&include_null_first_air_dates=false';
    de2016Url =
        '${Tmdb.urlSimple}discover/tv?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&timezone=America%2FArgentina%2FBuenos_Aires&first_air_date_year=2016&include_null_first_air_dates=false';
    de2015Url =
        '${Tmdb.urlSimple}discover/tv?api_key=${Tmdb.apiKey}&language=es&sort_by=popularity.desc&include_adult=false&page=1&timezone=America%2FArgentina%2FBuenos_Aires&first_air_date_year=2015&include_null_first_air_dates=false';
    _fetchtrendingTvWeek();
    _fetchAction();
    _fetchAdventure();
    _fetchAnimation();
    _fetchComedia();
    _fetchCrimen();
    _fetchDocumental();
    _fetchDrama();
    _fetchFantasia();
    _fetchFamilia();
    _fetchHistoria();
    _fetchTerror();
    _fetchMusica();
    _fetchMisterio();
    _fetchRomance();
    _fetchCienciaFiccion();
    _fetchSuspenso();
    _fetchBelica();
    _fetchWestern();
    _fetch2019();
    _fetch2018();
    _fetch2017();
    _fetch2016();
    _fetch2015();
    loadingPage = false;
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  void _fetchtrendingTvWeek() async {
    var response = await http.get(trendingTvWeekUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      trendingTvWeek = TvSeries.fromJson(decodeJson);
    });
  }

  void _fetchAction() async {
    var response = await http.get(actionUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      action = TvSeries.fromJson(decodeJson);
    });
  }

  void _fetchAdventure() async {
    var response = await http.get(adventureUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      adventure = TvSeries.fromJson(decodeJson);
    });
  }

  void _fetchAnimation() async {
    var response = await http.get(animationUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      animation = TvSeries.fromJson(decodeJson);
    });
  }

  void _fetchComedia() async {
    var response = await http.get(comediaUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      comedia = TvSeries.fromJson(decodeJson);
    });
  }

  void _fetchCrimen() async {
    var response = await http.get(crimenUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      crimen = TvSeries.fromJson(decodeJson);
    });
  }

  void _fetchDocumental() async {
    var response = await http.get(documentalUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      documental = TvSeries.fromJson(decodeJson);
    });
  }

  void _fetchDrama() async {
    var response = await http.get(dramaUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      drama = TvSeries.fromJson(decodeJson);
    });
  }

  void _fetchFamilia() async {
    var response = await http.get(familiaUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      familia = TvSeries.fromJson(decodeJson);
    });
  }

  void _fetchFantasia() async {
    var response = await http.get(fantasiaUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      fantasia = TvSeries.fromJson(decodeJson);
    });
  }

  void _fetchHistoria() async {
    var response = await http.get(historiaUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      historia = TvSeries.fromJson(decodeJson);
    });
  }

  void _fetchTerror() async {
    var response = await http.get(terrorUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      terror = TvSeries.fromJson(decodeJson);
    });
  }

  void _fetchMusica() async {
    var response = await http.get(musicaUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      musica = TvSeries.fromJson(decodeJson);
    });
  }

  void _fetchMisterio() async {
    var response = await http.get(misterioUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      misterio = TvSeries.fromJson(decodeJson);
    });
  }

  void _fetchRomance() async {
    var response = await http.get(romanceUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      romance = TvSeries.fromJson(decodeJson);
    });
  }

  void _fetchCienciaFiccion() async {
    var response = await http.get(cienciaFiccionUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      cienciaFiccion = TvSeries.fromJson(decodeJson);
    });
  }

  void _fetchSuspenso() async {
    var response = await http.get(suspensoUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      suspenso = TvSeries.fromJson(decodeJson);
    });
  }

  void _fetchBelica() async {
    var response = await http.get(belicasUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      belicas = TvSeries.fromJson(decodeJson);
    });
  }

  void _fetchWestern() async {
    var response = await http.get(westernUrl);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      western = TvSeries.fromJson(decodeJson);
    });
  }

  void _fetch2019() async {
    var response = await http.get(de2019Url);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      de2019 = TvSeries.fromJson(decodeJson);
    });
  }

  void _fetch2018() async {
    var response = await http.get(de2018Url);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      de2018 = TvSeries.fromJson(decodeJson);
    });
  }

  void _fetch2017() async {
    var response = await http.get(de2017Url);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      de2017 = TvSeries.fromJson(decodeJson);
    });
  }

  void _fetch2016() async {
    var response = await http.get(de2016Url);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      de2016 = TvSeries.fromJson(decodeJson);
    });
  }

  void _fetch2015() async {
    var response = await http.get(de2015Url);
    var decodeJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      de2015 = TvSeries.fromJson(decodeJson);
    });
  }

  Widget _buildTopContent() => Padding(
        padding:
            EdgeInsets.only(top: 18.0, left: 18.0, right: 18.0, bottom: 10.0),
        child: Container(
          height: MediaQuery.of(context).size.height * 0.30,
          width: double.infinity,
          color: Colors.transparent,
          child: Column(
            children: <Widget>[
              Text(
                'Conoce más sobre tus series favoritas.',
                style: TextStyle(
                  fontFamily: 'MontserratBold',
                  fontSize: 31.0,
                  color: Colors.white,
                ),
                textAlign: TextAlign.left,
              ),
              SizedBox(
                height: 20.0,
              ),
              Container(
                width: double.infinity,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    MaterialButton(
                      elevation: 5.0,
                      color: Color.fromRGBO(255, 0, 57, 1),
                      child: Text(
                        'EN EMISION',
                        style: TextStyle(
                            fontFamily: 'RalewayBold',
                            fontSize: 17.0,
                            color: Colors.white),
                      ),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => TvAiringToday()));
                      },
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    OutlineButton(
                      borderSide:
                          BorderSide(color: Color.fromRGBO(255, 0, 57, 1)),
                      child: Text(
                        'AL AIRE',
                        style: TextStyle(
                            fontFamily: 'RalewayBold',
                            fontSize: 17.0,
                            color: Colors.white),
                      ),
                      onPressed: () {
                        Navigator.push(
                            context,
                            CupertinoPageRoute(
                                builder: (context) => TvOnTheAir()));
                      },
                    )
                  ],
                ),
              ),
              Container(
                child: Row(
                  children: <Widget>[
                    OutlineButton(
                      borderSide:
                          BorderSide(color: Color.fromRGBO(255, 0, 57, 1)),
                      child: Text(
                        'POPULARES',
                        style: TextStyle(
                            fontFamily: 'RalewayBold',
                            fontSize: 17.0,
                            color: Colors.white),
                      ),
                      onPressed: () {
                        Navigator.push(
                            context,
                            CupertinoPageRoute(
                                builder: (context) => TvPopular()));
                      },
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    MaterialButton(
                      elevation: 5.0,
                      color: Color.fromRGBO(255, 0, 57, 1),
                      child: Text(
                        'MEJOR PUNTUADAS',
                        style: TextStyle(
                            fontFamily: 'RalewayBold',
                            fontSize: 17.0,
                            color: Colors.white),
                      ),
                      onPressed: () {
                        Navigator.push(
                            context,
                            CupertinoPageRoute(
                                builder: (context) => TvTopRated()));
                      },
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      );

  Widget _buildMovieListItemCard(Results tvItem) => Container(
        child: Padding(
          padding: const EdgeInsets.only(top: 6.0, bottom: 6.0),
          child: Column(
            children: <Widget>[
              Card(
                elevation: 10.0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                child: Container(
                    height: 200.0,
                    width: MediaQuery.of(context).size.width * .9,
                    child: Stack(children: <Widget>[
                      new ClipRRect(
                        borderRadius: new BorderRadius.circular(7.0),
                        child: InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => TvDetailPage(
                                            tvSerie: tvItem,
                                          )));
                            },
                            child: tvItem.backdropPath == null
                                ? Container(
                                    height: 180.0,
                                    width: double.infinity,
                                    color: Color.fromRGBO(35, 47, 52, 1),
                                    child: Icon(
                                      Icons.movie,
                                      color: Colors.grey,
                                    ))
                                : Image.network(
                                    '${Tmdb.baseImageUrl}w780${tvItem.backdropPath}',
                                    fit: BoxFit.cover,
                                    width: double.infinity,
                                    height: 200.0,
                                  )),
                      ),
                      Positioned(
                          top: 3.0,
                          right: 3.0,
                          child: tvItem.firstAirDate == null ||
                                  tvItem.firstAirDate == ""
                              ? Container(
                                  height: 15.0,
                                  width: 30.0,
                                  decoration: BoxDecoration(
                                      color: Color.fromRGBO(255, 0, 57, .9),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(7.5))),
                                  child: Center(
                                      child: Text(
                                    "s/d",
                                    style: TextStyle(
                                      fontSize: 11.0,
                                      fontFamily: 'RalewayLight',
                                      color: Colors.grey,
                                    ),
                                    textScaleFactor: 1.0,
                                  )),
                                )
                              : Container(
                                  height: 15.0,
                                  width: 30.0,
                                  decoration: BoxDecoration(
                                      color: Color.fromRGBO(255, 0, 57, .9),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(7.5))),
                                  child: Center(
                                    child: Text(
                                      "${dt.format(DateTime.tryParse(tvItem.firstAirDate))}",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: 'RalewayMedium',
                                        fontSize: 10.0,
                                      ),
                                      textAlign: TextAlign.center,
                                      textScaleFactor: 1.0,
                                    ),
                                  ),
                                )),
                      Positioned(
                        bottom: 15.0,
                        left: 15.0,
                        child: Container(
                          width: MediaQuery.of(context).size.width * .8,
                          child: Text(
                            tvItem.name,
                            style: TextStyle(
                                fontFamily: 'MontserratMedium',
                                fontSize: 20.0,
                                color: Colors.white),
                            textScaleFactor: 1.0,
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.center,
                            maxLines: 2,
                          ),
                        ),
                      ),
                    ])),
              ),
            ],
          ),
        ),
      );

  Widget _buildCardsListCard(TvSeries tvSeries, String movieListTitle,
          String codeList, String sortBy, String year) =>
      Container(
        height: 265.0,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 4.0, bottom: 4.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: 30.0,
                        width: 1.0,
                        color: Color.fromRGBO(255, 0, 57, 1),
                      ),
                      SizedBox(
                        width: 6.0,
                      ),
                      Text(
                        movieListTitle,
                        style: TextStyle(
                            fontSize: 17.0,
                            fontFamily: 'MontserratMedium',
                            color: Colors.grey[200]),
                        textScaleFactor: 1.0,
                      ),
                    ],
                  ),
                )
              ],
            ),
            Flexible(
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: tvSeries == null
                    ? <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left: 38.0),
                          child: Center(
                              child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              CircularProgressIndicator(
                                valueColor: new AlwaysStoppedAnimation<Color>(
                                    Color.fromRGBO(255, 0, 57, 1)),
                              ),
                              SizedBox(
                                height: 3.0,
                              ),
                              Text(
                                'Cargando..',
                                style: TextStyle(
                                    fontFamily: 'RalewayMedium',
                                    color: Colors.white,
                                    fontSize: 16.0),
                              )
                            ],
                          )),
                        )
                      ]
                    : tvSeries.results
                        .map((tvItem) => Padding(
                              padding: EdgeInsets.only(left: 6.0, right: 2.0),
                              child: _buildMovieListItemCard(tvItem),
                            ))
                        .toList(),
              ),
            )
          ],
        ),
      );

  Widget _buildScrollTvView() => ListView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          _buildTopContent(),
          SizedBox(
            height: 20.0,
          ),
          _buildCardsListCard(trendingTvWeek, 'Tendencias del dia', '', '', ''),
          SizedBox(
            height: 30.0,
          ),
          CardsListTv(
              itemTv: de2019, listTitle: 'Películas de 2019', codeList: '', sortBy: 'release_date.asc', year: '2019', loading: loadingPage),
          SizedBox(
            height: 30.0,
          ),
          CardsListTv(
              itemTv: action, listTitle: 'Acción', codeList: Tmdb.actionCode, sortBy: 'popularity.desc', year: '', loading: loadingPage),
          SizedBox(
            height: 30.0,
          ),
          CardsListTv(
              itemTv: adventure, listTitle: 'Aventura', codeList: Tmdb.adventureCode, sortBy: 'popularity.desc', year: '', loading: loadingPage),
          SizedBox(
            height: 30.0,
          ),
          CardsListTv(
              itemTv: belicas, listTitle: 'Bélicas', codeList: Tmdb.belicaCode, sortBy: 'popularity.desc', year: '', loading: loadingPage),
          SizedBox(
            height: 30.0,
          ),
          CardsListTv(
              itemTv: de2018, listTitle: 'Películas de 2018', codeList: '', sortBy: 'popularity.desc', year: '2018', loading: loadingPage),
          SizedBox(
            height: 30.0,
          ),
          CardsListTv(
              itemTv: animation, listTitle: 'Animación', codeList: Tmdb.animacionCode, sortBy: 'popularity.desc', year: '', loading: loadingPage),
          SizedBox(
            height: 30.0,
          ),
          CardsListTv(
              itemTv: cienciaFiccion, listTitle: 'Ciencia ficción', codeList: Tmdb.cienciaFiccionCode, sortBy: 'popularity.desc', year: '', loading: loadingPage),
          SizedBox(
            height: 30.0,
          ),
          CardsListTv(
              itemTv:comedia, listTitle: 'Comedia', codeList: Tmdb.comediaCode, sortBy: 'popularity.desc', year: '', loading: loadingPage),
          SizedBox(
            height: 30.0,
          ),
          CardsListTv(
              itemTv: de2017, listTitle: 'Películas de 2017', codeList: '', sortBy: 'popularity.desc', year: '2017', loading: loadingPage),
          SizedBox(
            height: 30.0,
          ),
          CardsListTv(
              itemTv: crimen, listTitle: 'Crimen', codeList: Tmdb.crimenCode, sortBy: 'popularity.desc', year: '', loading: loadingPage),
          SizedBox(
            height: 30.0,
          ),
          CardsListTv(
              itemTv: documental, listTitle: 'Documentales', codeList: Tmdb.documentalCode, sortBy: 'popularity.desc', year: '', loading: loadingPage),
          SizedBox(
            height: 30.0,
          ),
          CardsListTv(
              itemTv: drama, listTitle: 'Drama', codeList: Tmdb.dramaCode, sortBy: 'popularity.desc', year: '', loading: loadingPage),
          SizedBox(
            height: 30.0,
          ),
          CardsListTv(
              itemTv: familia, listTitle: 'Família', codeList: Tmdb.familiaCode, sortBy: 'popularity.desc', year: '', loading: loadingPage),
          SizedBox(
            height: 30.0,
          ),
          CardsListTv(
              itemTv: de2016, listTitle: 'Películas de 2016', codeList: '', sortBy: 'popularity.desc', year: '2016', loading: loadingPage),
          SizedBox(
            height: 30.0,
          ),
          CardsListTv(
              itemTv: fantasia, listTitle: 'Fantasía', codeList: Tmdb.fantasiaCode, sortBy: 'popularity.desc', year: '', loading: loadingPage),
          SizedBox(
            height: 30.0,
          ),
          CardsListTv(
              itemTv: historia, listTitle: 'Historia', codeList: Tmdb.historiaCode, sortBy: 'popularity.desc', year: '', loading: loadingPage),
          SizedBox(
            height: 30.0,
          ),
          CardsListTv(
              itemTv: misterio, listTitle: 'Misterio', codeList: Tmdb.misterioCode, sortBy: 'popularity.desc', year: '', loading: loadingPage),
          SizedBox(
            height: 30.0,
          ),
          CardsListTv(
              itemTv: musica, listTitle: 'Música', codeList: Tmdb.musicaCode, sortBy: 'popularity.desc', year: '', loading: loadingPage),
          SizedBox(
            height: 30.0,
          ),
          CardsListTv(
              itemTv: de2015, listTitle: 'Películas de 2015', codeList: '', sortBy: 'popularity.desc', year: '2015', loading: loadingPage),
          SizedBox(
            height: 30.0,
          ),
          CardsListTv(
              itemTv: romance, listTitle: 'Romance', codeList: Tmdb.romanceCode, sortBy: 'popularity.desc', year: '', loading: loadingPage),
          SizedBox(
            height: 30.0,
          ),
          CardsListTv(
              itemTv: suspenso, listTitle: 'Suspenso', codeList: Tmdb.suspensoCode, sortBy: 'popularity.desc', year: '', loading: loadingPage),
          SizedBox(
            height: 30.0,
          ),
          CardsListTv(
              itemTv: terror, listTitle: 'Terror', codeList: Tmdb.terrorCode, sortBy: 'popularity.desc', year: '', loading: loadingPage),
          SizedBox(
            height: 30.0,
          ),
          CardsListTv(
              itemTv: western, listTitle: 'Western', codeList: Tmdb.westernCode, sortBy: 'popularity.desc', year: '', loading: loadingPage),
        ],
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(26, 27, 31, 1),
      body: loadingPage == true
          ? <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 38.0),
                child: Center(
                    child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation<Color>(
                          Color.fromRGBO(255, 0, 57, 1)),
                    ),
                    SizedBox(
                      height: 3.0,
                    ),
                    Text(
                      'Cargando..',
                      style: TextStyle(
                          fontFamily: 'RalewayMedium',
                          color: Colors.white,
                          fontSize: 16.0),
                    )
                  ],
                )),
              )
            ]
          : _buildScrollTvView(),
    );
  }
}
