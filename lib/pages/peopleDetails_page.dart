import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:muvi_app/helpers/loader.dart';
import 'package:muvi_app/models/movieModel.dart';
import 'package:muvi_app/models/peopleDetailsModel.dart';
import 'package:muvi_app/models/peopleMovieCredits.dart';
import 'package:muvi_app/models/peopleTvCredits.dart';
import 'package:muvi_app/models/tmdb.dart';
import 'package:muvi_app/models/tvSeriesModel.dart';
import 'package:muvi_app/pages/imdb_page.dart';
import 'package:muvi_app/pages/tmdb_page.dart';
import 'package:muvi_app/pages/movieDetails_page.dart';
import 'package:muvi_app/pages/tvDetails_page.dart';
import 'package:muvi_app/pages/webSite_page.dart';

class PeoplePage extends StatefulWidget {
  final int person;

  PeoplePage({this.person});

  _PeoplePageState createState() => _PeoplePageState();
}

class _PeoplePageState extends State<PeoplePage> {
  String personDetailsUrl;
  String movieCreditsUrl;
  String tvCreditsUrl;
  PeopleCredits movieCredits;
  PeopleTvCredits tvCredits;
  PeopleDetails peopleDetails;
  bool charging = false;

  final dt = DateFormat('yyyy');

  @override
  void initState() {
    super.initState();
    personDetailsUrl =
        '${Tmdb.personUrl}${widget.person}?api_key=${Tmdb.apiKey}';
    movieCreditsUrl =
        '${Tmdb.urlSimple}person/${widget.person}/movie_credits?api_key=${Tmdb.apiKey}&language=es';
    tvCreditsUrl =
        '${Tmdb.urlSimple}person/${widget.person}/tv_credits?api_key=${Tmdb.apiKey}&language=es';
    _fetchPeopleDetails();
    _fetchMovieCredits();
    _fetchTvCredits();
  }

  void _fetchPeopleDetails() async {
    var response = await http.get(personDetailsUrl);
    var decodedJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      peopleDetails = PeopleDetails.fromJson(decodedJson);
    });
  }

  void _fetchMovieCredits() async {
    var response = await http.get(movieCreditsUrl);
    var decodedJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      movieCredits = PeopleCredits.fromJson(decodedJson);
    });
  }

  void _fetchTvCredits() async {
    var response = await http.get(tvCreditsUrl);
    var decodedJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      tvCredits = PeopleTvCredits.fromJson(decodedJson);
    });
  }

  Widget _buildInfoPeople() => Padding(
        padding: const EdgeInsets.only(left: 10.0, right: 10.0),
        child: Container(
          width: double.infinity,
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  peopleDetails.name,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 28.0,
                      fontFamily: "MontserratBold"),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    MaterialButton(
                        height: 30.0,
                        minWidth: MediaQuery.of(context).size.width * .2,
                        elevation: 5.0,
                        color: Color.fromRGBO(232, 183, 8, 1),
                        child: Text(
                          'IMDb',
                          style: TextStyle(
                              fontSize: 18.0, fontWeight: FontWeight.w900),
                        ),
                        onPressed: () {
                          if (peopleDetails.imdbId != "") {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ImdbPage(
                                          idImdb:
                                              'name/${peopleDetails.imdbId}',
                                          name: peopleDetails.name,
                                        )));
                          }
                        }),
                    SizedBox(
                      width: 5.0,
                    ),
                    MaterialButton(
                        height: 30.0,
                        minWidth: MediaQuery.of(context).size.width * .2,
                        elevation: 5.0,
                        color: Color.fromRGBO(1, 210, 119, 1),
                        child: Image.asset(
                          'assets/tmdb_logo.png',
                          fit: BoxFit.cover,
                          height: 25.0,
                        ),
                        onPressed: () {
                          if (peopleDetails.imdbId != "") {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => TmdbPage(
                                          idTmdb: 'person/${peopleDetails.id}',
                                          name: peopleDetails.name,
                                        )));
                          }
                        }),
                    SizedBox(
                      width: 5.0,
                    ),
                    Container(
                      child: peopleDetails.homepage == "" ||
                              peopleDetails.homepage == null
                          ? MaterialButton(
                              height: 30.0,
                              minWidth: MediaQuery.of(context).size.width * .5,
                              elevation: 5.0,
                              color: Colors.grey,
                              child: Text('SIN SITIO WEB',
                                  style: TextStyle(
                                      fontSize: 18.0, color: Colors.white)),
                              onPressed: () {})
                          : MaterialButton(
                              height: 30.0,
                              minWidth: MediaQuery.of(context).size.width * .5,
                              elevation: 5.0,
                              color: Color.fromRGBO(255, 0, 57, 1),
                              child: Text('SITIO WEB',
                                  style: TextStyle(
                                      fontSize: 18.0, color: Colors.white)),
                              onPressed: () {
                                if (peopleDetails.homepage != "") {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => WebSitePage(
                                                urlWeb: peopleDetails.homepage,
                                                name: peopleDetails.name,
                                              )));
                                }
                              }),
                    )
                  ],
                ),
                SizedBox(
                  height: 5.0,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: 30.0,
                      width: 1.0,
                      color: Color.fromRGBO(255, 0, 57, 1),
                    ),
                    SizedBox(width: 6.0),
                    Text('Nacimiento ',
                        style: TextStyle(
                          fontSize: 20.0,
                          fontFamily: 'MontserratMedium',
                          color: Colors.white,
                        )),
                  ],
                ),
                SizedBox(
                  height: 5.0,
                ),
                Padding(
                    padding: const EdgeInsets.only(left: 8.0, right: 6.0),
                    child: Container(
                      child: peopleDetails == null ||
                              peopleDetails.birthday == null ||
                              peopleDetails.placeOfBirth == null
                          ? Text('Sin informacion de nacimiento',
                              style: TextStyle(
                                fontSize: 15.0,
                                fontFamily: 'RalewayMedium',
                                color: Color.fromRGBO(109, 115, 125, 1),
                              ))
                          : Text(
                              "${DateFormat('dd/MM/yyyy').format(DateTime.parse(peopleDetails.birthday))} - ${peopleDetails.placeOfBirth}",
                              style: TextStyle(
                                fontSize: 15.0,
                                fontFamily: 'RalewayMedium',
                                color: Color.fromRGBO(109, 115, 125, 1),
                              )),
                    )),
                SizedBox(
                  height: 15.0,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: 30.0,
                      width: 1.0,
                      color: Color.fromRGBO(255, 0, 57, 1),
                    ),
                    SizedBox(width: 6.0),
                    Text('Biografía ',
                        style: TextStyle(
                          fontSize: 20.0,
                          fontFamily: 'MontserratMedium',
                          color: Colors.white,
                        )),
                  ],
                ),
                SizedBox(
                  height: 5.0,
                ),
                Padding(
                    padding: const EdgeInsets.only(left: 8.0, right: 6.0),
                    child: Container(
                      child: peopleDetails == null
                          ? Text(
                              'No hay biografia disponible en este idioma',
                              style: TextStyle(
                                fontSize: 15.0,
                                color: Color.fromRGBO(109, 115, 125, 1),
                                fontFamily: 'RalewayMedium',
                              ),
                            )
                          : Text(
                              peopleDetails.biography,
                              style: TextStyle(
                                fontSize: 15.0,
                                color: Color.fromRGBO(109, 115, 125, 1),
                                fontFamily: 'RalewayMedium',
                              ),
                            ),
                    )),
                SizedBox(
                  height: 15.0,
                ),
              ]),
        ),
      );

  Widget _buildTopPart() => Container(
        height: MediaQuery.of(context).size.height * .45,
        child: Stack(
          children: <Widget>[
            ClipPath(
              clipper: Mclipper(),
              child: Container(
                height: MediaQuery.of(context).size.height * .45,
                decoration: BoxDecoration(color: Colors.white, boxShadow: [
                  BoxShadow(
                      color: Colors.black12,
                      offset: Offset(0.0, 10.0),
                      blurRadius: 10.0)
                ]),
                child: Stack(
                  children: <Widget>[
                    Container(
                        child: peopleDetails == null ||
                                peopleDetails.profilePath == null
                            ? Image.asset('assets/nobody.jpg',
                                fit: BoxFit.cover)
                            : Image.network(
                                '${Tmdb.baseImageUrl}original${peopleDetails.profilePath}',
                                fit: BoxFit.cover,
                                width: double.infinity,
                                height:
                                    MediaQuery.of(context).size.height * .45,
                              )),
                  ],
                ),
              ),
            ),
            Positioned(
              top: 10.0,
              left: 10.0,
              child: BackButton(
                color: Colors.white,
              ),
            )
          ],
        ),
      );

  Widget _buildMovieListItem(Movies movieItem) => Container(
        child: Padding(
          padding: const EdgeInsets.only(top: 6.0, bottom: 6.0),
          child: Column(
            children: <Widget>[
              Card(
                elevation: 10.0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                child: Container(
                    height: 180.0,
                    width: 120.0,
                    child: Stack(children: <Widget>[
                      new ClipRRect(
                        borderRadius: new BorderRadius.circular(7.0),
                        child: InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => MovieDetailPage(
                                            movie: movieItem,
                                          )));
                            },
                            child: movieItem.posterPath == null
                                ? Container(
                                    height: 180.0,
                                    width: double.infinity,
                                    color: Color.fromRGBO(35, 47, 52, 1),
                                    child: Icon(
                                      Icons.movie,
                                      color: Colors.grey,
                                    ))
                                : Image.network(
                                    '${Tmdb.baseImageUrl}w342${movieItem.posterPath}',
                                    fit: BoxFit.cover,
                                    width: double.infinity,
                                    height: 200.0,
                                  )),
                      ),
                      Positioned(
                          top: 3.0,
                          right: 3.0,
                          child: movieItem.releaseDate == null ||
                                  movieItem.releaseDate == ""
                              ? Container(
                                  height: 15.0,
                                  width: 30.0,
                                  decoration: BoxDecoration(
                                      color: Color.fromRGBO(255, 0, 57, .9),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(7.5))),
                                  child: Center(
                                      child: Text(
                                    "s/d",
                                    style: TextStyle(
                                      fontSize: 11.0,
                                      fontFamily: 'RalewayLight',
                                      color: Colors.grey,
                                    ),
                                    textScaleFactor: 1.0,
                                  )),
                                )
                              : Container(
                                  height: 15.0,
                                  width: 30.0,
                                  decoration: BoxDecoration(
                                      color: Color.fromRGBO(255, 0, 57, .9),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(7.5))),
                                  child: Center(
                                    child: Text(
                                      "${dt.format(DateTime.tryParse(movieItem.releaseDate))}",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: 'RalewayMedium',
                                        fontSize: 10.0,
                                      ),
                                      textAlign: TextAlign.center,
                                      textScaleFactor: 1.0,
                                    ),
                                  ),
                                ))
                    ])),
              ),
              Padding(
                padding: EdgeInsets.only(top: 4.0),
                child: Container(
                  width: 110.0,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Center(
                        child: Text(
                          movieItem.title,
                          style: TextStyle(
                              fontFamily: 'MontserratRegular',
                              fontSize: 10.0,
                              color: Colors.white70),
                          textScaleFactor: 1.0,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );

  Widget _buildCardsListActing(
          PeopleCredits movies, String movieListTitle) =>
      Container(
        height: 265.0,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 10.0, bottom: 4.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: 30.0,
                        width: 1.0,
                        color: Color.fromRGBO(255, 0, 57, 1),
                      ),
                      SizedBox(
                        width: 12.0,
                      ),
                      Text(
                        movieListTitle,
                        style: TextStyle(
                            fontSize: 17.0,
                            fontFamily: 'MontserratMedium',
                            color: Colors.grey[200]),
                        textScaleFactor: 1.0,
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Flexible(
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: movies == null
                    ? <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left: 38.0),
                          child: Center(
                              child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              CircularProgressIndicator(
                                valueColor: new AlwaysStoppedAnimation<Color>(
                                    Color.fromRGBO(255, 0, 57, 1)),
                              ),
                              SizedBox(
                                height: 3.0,
                              ),
                              Text(
                                'Cargando..',
                                style: TextStyle(
                                    fontFamily: 'RalewayMedium',
                                    color: Colors.white,
                                    fontSize: 16.0),
                              )
                            ],
                          )),
                        )
                      ]
                    : movieCredits.cast
                        .map((movieItem) => Padding(
                              padding: EdgeInsets.only(
                                  top: 6.0, left: 6.0, right: 2.0),
                              child: _buildMovieListItem(movieItem),
                            ))
                        .toList(),
              ),
            )
          ],
        ),
      );
  
  Widget _buildMovieListItemTv(Results tvItem) => Container(
        child: Padding(
          padding: const EdgeInsets.only(top: 6.0, bottom: 6.0),
          child: Column(
            children: <Widget>[
              Card(
                elevation: 10.0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                child: Container(
                    height: 180.0,
                    width: 120.0,
                    child: Stack(children: <Widget>[
                      new ClipRRect(
                        borderRadius: new BorderRadius.circular(7.0),
                        child: InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => TvDetailPage(
                                            tvSerie: tvItem,
                                          )));
                            },
                            child: tvItem.posterPath == null
                                ? Container(
                                    height: 180.0,
                                    width: double.infinity,
                                    color: Color.fromRGBO(35, 47, 52, 1),
                                    child: Icon(
                                      Icons.movie,
                                      color: Colors.grey,
                                    ))
                                : Image.network(
                                    '${Tmdb.baseImageUrl}w342${tvItem.posterPath}',
                                    fit: BoxFit.cover,
                                    width: double.infinity,
                                    height: 200.0,
                                  )),
                      ),
                      Positioned(
                          top: 3.0,
                          right: 3.0,
                          child: tvItem.firstAirDate == null ||
                                  tvItem.firstAirDate == ""
                              ? Container(
                                  height: 15.0,
                                  width: 30.0,
                                  decoration: BoxDecoration(
                                      color: Color.fromRGBO(255, 0, 57, .9),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(7.5))),
                                  child: Center(
                                      child: Text(
                                    "s/d",
                                    style: TextStyle(
                                      fontSize: 11.0,
                                      fontFamily: 'RalewayLight',
                                      color: Colors.grey,
                                    ),
                                    textScaleFactor: 1.0,
                                  )),
                                )
                              : Container(
                                  height: 15.0,
                                  width: 30.0,
                                  decoration: BoxDecoration(
                                      color: Color.fromRGBO(255, 0, 57, .9),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(7.5))),
                                  child: Center(
                                    child: Text(
                                      "${dt.format(DateTime.tryParse(tvItem.firstAirDate))}",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: 'RalewayMedium',
                                        fontSize: 10.0,
                                      ),
                                      textAlign: TextAlign.center,
                                      textScaleFactor: 1.0,
                                    ),
                                  ),
                                ))
                    ])),
              ),
              Padding(
                padding: EdgeInsets.only(top: 4.0),
                child: Container(
                  width: 110.0,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Center(
                        child: Text(
                          tvItem.name,
                          style: TextStyle(
                              fontFamily: 'MontserratRegular',
                              fontSize: 10.0,
                              color: Colors.white70),
                          textScaleFactor: 1.0,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );

  Widget _buildCardsListTv(
          PeopleTvCredits tvSeries, String movieListTitle) =>
      Container(
        height: 265.0,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 10.0, bottom: 4.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: 30.0,
                        width: 1.0,
                        color: Color.fromRGBO(255, 0, 57, 1),
                      ),
                      SizedBox(
                        width: 12.0,
                      ),
                      Text(
                        movieListTitle,
                        style: TextStyle(
                            fontSize: 17.0,
                            fontFamily: 'MontserratMedium',
                            color: Colors.grey[200]),
                        textScaleFactor: 1.0,
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Flexible(
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: tvSeries == null
                    ? <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left: 38.0),
                          child: Center(
                              child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              CircularProgressIndicator(
                                valueColor: new AlwaysStoppedAnimation<Color>(
                                    Color.fromRGBO(255, 0, 57, 1)),
                              ),
                              SizedBox(
                                height: 3.0,
                              ),
                              Text(
                                'Cargando..',
                                style: TextStyle(
                                    fontFamily: 'RalewayMedium',
                                    color: Colors.white,
                                    fontSize: 16.0),
                              )
                            ],
                          )),
                        )
                      ]
                    : tvCredits.cast
                        .map((tvItem) => Padding(
                              padding: EdgeInsets.only(
                                  top: 6.0, left: 6.0, right: 2.0),
                              child: _buildMovieListItemTv(tvItem),
                            ))
                        .toList(),
              ),
            )
          ],
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color.fromRGBO(26, 27, 31, 1),
        body: peopleDetails == null
            ? Center(
                child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Loader(),
                  Text(
                    'Cargando informacion de persona',
                    style: TextStyle(
                        fontFamily: 'RalewayMedium',
                        color: Colors.white,
                        fontSize: 16.0),
                  )
                ],
              ))
            : ListView(children: [
                _buildTopPart(),
                SizedBox(
                  height: 15.0,
                ),
                _buildInfoPeople(),
                SizedBox(height: 30.0),
                _buildCardsListActing(movieCredits, 'Filmografia'),
                _buildCardsListTv(tvCredits, 'Series de Tv')
              ]));
  }
}

class Mclipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();
    path.lineTo(100.0, 0.0);

    var controlpoint = Offset(50.0, 50.0);
    var endpoint = Offset(0.0, 100.0);

    path.quadraticBezierTo(
        controlpoint.dx, controlpoint.dy, endpoint.dx, endpoint.dy);

    path.lineTo(0.0, size.height);
    path.lineTo(size.width - 100.0, size.height);
    path.lineTo(size.width, size.height - 100.0);
    path.lineTo(size.width, 0.0);

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
