import 'package:flutter/material.dart';
import 'package:muvi_app/models/tmdb.dart';
import 'package:photo_view/photo_view.dart';

class ImagesPage extends StatefulWidget {
  final filePath;

  ImagesPage({Key key, this.filePath}) : super(key: key);

  @override
  _ImagesPageState createState() => _ImagesPageState();
}

class _ImagesPageState extends State<ImagesPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(26, 27, 31, 1),
      appBar: new AppBar(
        backgroundColor: Color.fromRGBO(26, 27, 31, 1),
        elevation: 0.0,
      ),
      body: Hero(
        tag: widget.filePath,
        child: Center(
          /* heightFactor: 2.5, */
          child: ClipRRect(
            borderRadius: BorderRadius.circular(7.0),
            child: Container(
              /*  height: MediaQuery.of(context).size.height *.8, */
              width: MediaQuery.of(context).size.width,
              child: PhotoView(
                backgroundDecoration: BoxDecoration(color: Colors.transparent),
                imageProvider: NetworkImage(
                  '${Tmdb.baseImageUrl}w1280${widget.filePath}',
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
