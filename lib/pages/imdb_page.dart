import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class ImdbPage extends StatefulWidget {
  final String idImdb;
  final String name;

  ImdbPage({this.idImdb, this.name});

  _ImdbPageState createState() => _ImdbPageState();
}

class _ImdbPageState extends State<ImdbPage> {
  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(18, 19, 23, 1),
        title: Text(
          'Imdb - ${widget.name}',
          style: TextStyle(
            fontSize: 18.0,
            color: Colors.white,
            fontFamily: 'MontserratMedium',
          ),
        ),
      ),
      withZoom: true,
      url: 'https://www.imdb.com/${widget.idImdb}',
    );
  }
}
