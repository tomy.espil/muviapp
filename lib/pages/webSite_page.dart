import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class WebSitePage extends StatefulWidget {
  final String urlWeb;
  final String name;

  WebSitePage({this.urlWeb, this.name});

  _WebSitePageState createState() => _WebSitePageState();
}

class _WebSitePageState extends State<WebSitePage> {
  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(18, 19, 23, 1),
        title: Text(
          'Sitio Web - ${widget.name}',
          style: TextStyle(
            fontSize: 18.0,
            color: Colors.white,
            fontFamily: 'MontserratMedium',
          ),
        ),
      ),
      withZoom: true,
      url: widget.urlWeb,
    );
  }
}