import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import 'package:muvi_app/models/tmdb.dart';
import 'package:muvi_app/models/tvDetailsModel.dart';
import 'package:muvi_app/pages/episodes_page.dart';

class SeasonsPage extends StatefulWidget {
  final int idShow;

  SeasonsPage({this.idShow});
  _SeasonsPageState createState() => _SeasonsPageState();
}

class _SeasonsPageState extends State<SeasonsPage> {
  String tvDetailUrl;
  TvDetailsModel tvDetails;

  @override
  void initState() {
    super.initState();
    tvDetailUrl =
        '${Tmdb.baseUrlTv}${widget.idShow}?api_key=${Tmdb.apiKey}&language=Es';
    _fetchTvDetails();
  }

  void _fetchTvDetails() async {
    var response = await http.get(tvDetailUrl);
    var decodedJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      tvDetails = TvDetailsModel.fromJson(decodedJson);
    });
  }

  Widget _buildListItem() => Container(
        child: Padding(
            padding: const EdgeInsets.only(
                top: 5.0, bottom: 5.0, right: 8.0, left: 8.0),
            child: ListView(
              scrollDirection: Axis.vertical,
              children: tvDetails == null
                  ? <Widget>[Center(child: CircularProgressIndicator())]
                  : tvDetails.seasons
                      .map((s) => Container(
                        child: Padding(
                          padding: EdgeInsets.only(top: 5.0),
                          child: InkWell(
                            child: Container(
                                height: 130.0,
                                width: double.infinity,
                                child: Card(
                                  color: Color.fromRGBO(13, 13, 15, 1),
                                  elevation: 4.0,
                                  child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                            height: 130.0,
                                            width: MediaQuery.of(context).size.width * .25,
                                            child: Stack(
                                              children: <Widget>[
                                                Container(
                                                child: tvDetails.seasons == null
                                                  ? Container(
                                                      height: 180.0,
                                                      width: double.infinity,
                                                      color: Color.fromRGBO(35, 47, 52, 1),
                                                      child: Icon(
                                                        Icons.movie,
                                                        color: Colors.grey,
                                                      ))
                                                  : Image.network(
                                                      '${Tmdb.baseImageUrl}w342${s.posterPath}',
                                                      fit: BoxFit
                                                          .cover,
                                                      width: double
                                                          .infinity,
                                                      height: 130.0,
                                                    )),
                                                Positioned(
                                                  right: 8.0,
                                                  bottom: 6.0,
                                                  child: Text(
                                                    tvDetails == null || tvDetails.numberOfSeasons == 0
                                                          ? ""
                                                          : s.seasonNumber.toString(),
                                                      style: TextStyle(
                                                          fontFamily:'MontserratBold',
                                                          fontSize: 40.0,
                                                          color: Colors.grey)),
                                                ),
                                                Positioned(
                                                  right: 7.0,
                                                  bottom: 7.0,
                                                  child: Text(
                                                    tvDetails == null || tvDetails.numberOfSeasons == 0
                                                          ? ""
                                                          : s.seasonNumber.toString(),
                                                      style: TextStyle(
                                                          fontFamily:'MontserratBold',
                                                          fontSize: 40.0,
                                                          color: Colors.amber)),
                                                ),
                                              ],
                                            )),
                                        Padding(
                                          padding: const EdgeInsets.only(left: 8.0, top: 8),
                                          child: Column(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              children: <Widget>[
                                                Text(s.name,
                                                  style: TextStyle(
                                                    fontFamily: 'MontserratBold',
                                                    color: Colors.white,
                                                    fontSize: 15.0,
                                                  ),
                                                  textScaleFactor: 1.0,
                                                ),
                                                SizedBox(height: 3.0,),
                                                Text('${s.episodeCount.toString()} episodios',
                                                  style:TextStyle(
                                                    fontFamily: 'RalewayRegular',
                                                    color: Colors.grey[350],
                                                    fontSize: 12.0
                                                  ),
                                                  textScaleFactor: 1.0,
                                                ),
                                                SizedBox(height: 7.0,),
                                                Container(
                                                  width: MediaQuery.of(context).size.width *.6,
                                                  child: s.overview == ""
                                                  ? Text(
                                                      'Sinopsis desconocida',
                                                      style: TextStyle(
                                                        fontSize: 11.0,
                                                        fontFamily: 'RalewayLight',
                                                        color: Colors.white70,
                                                      ),
                                                      textScaleFactor: 1.0,
                                                      textAlign: TextAlign.center,
                                                    )
                                                  : Text(
                                                      s.overview,
                                                      style: TextStyle(
                                                        fontSize: 11.0,
                                                        fontFamily: 'RalewayLight',
                                                        color: Colors.white70,
                                                        fontStyle: FontStyle.italic
                                                      ),
                                                      maxLines: 5,
                                                      overflow: TextOverflow.ellipsis,
                                                      textAlign: TextAlign.center,
                                                      textScaleFactor: 1.0,
                                                    ))
                                              ],
                                            ),
                                        ),
                                      ]),
                                )
                            ),
                            onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => EpisodesPage(
                                            )));
                              },
                          )
                        )
                      )
                      )
                      .toList(),
            )),
      );

  @override
  Widget build(BuildContext context) {
    return _buildListItem();
  }
}
