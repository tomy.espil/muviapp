import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:muvi_app/models/tmdb.dart';
import 'package:muvi_app/models/videosModel.dart';
import 'package:http/http.dart' as http;
import 'package:muvi_app/pages/videosWebView_page.dart';

class VideosPage extends StatefulWidget {
  final int movieId;
  final String name;

  VideosPage({this.movieId, this.name});

  _VideosPageState createState() => _VideosPageState();
}

class _VideosPageState extends State<VideosPage> {
  String urlVideosEsp;
  String urlVideosIng;
  VideosModel videosModelEsp;
  VideosModel videosModelIng;

  @override
  void initState() {
    super.initState();
    urlVideosEsp =
        '${Tmdb.baseUrl}${widget.movieId}/videos?api_key=${Tmdb.apiKey}&language=es';
    _fetchVideosEsp();
    urlVideosIng =
        '${Tmdb.baseUrl}${widget.movieId}/videos?api_key=${Tmdb.apiKey}&language=en';
    _fetchVideosIng();
  }

  void _fetchVideosEsp() async {
    var response = await http.get(urlVideosEsp);
    var decodedJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      videosModelEsp = VideosModel.fromJson(decodedJson);
    });
  }

  void _fetchVideosIng() async {
    var response = await http.get(urlVideosIng);
    var decodedJson = jsonDecode(response.body);
    if (!mounted) return;
    setState(() {
      videosModelIng = VideosModel.fromJson(decodedJson);
    });
  }

  Widget _buildListVideos(VideosModel videoModel) => Column(
        children: videoModel == null
            ? <Widget>[
                Center(
                  child: CircularProgressIndicator(
                    valueColor: new AlwaysStoppedAnimation<Color>(
                        Color.fromRGBO(255, 0, 57, 1)),
                  ),
                )
              ]
            : videoModel.results
                .map((v) => Padding(
                      padding: const EdgeInsets.only(
                          left: 7.0, right: 7.0, bottom: 15.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(bottom: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      height: 20.0,
                                      width: 1.0,
                                      color: Color.fromRGBO(255, 0, 57, 1),
                                    ),
                                    SizedBox(
                                      width: 4.0,
                                    ),
                                    Text(
                                      v.type,
                                      style: TextStyle(
                                        color: Colors.white54,
                                        fontSize: 15.0,
                                        fontFamily: 'MontserratRegular',
                                      ),
                                      textScaleFactor: 1.0,
                                    ),
                                    Text(
                                      ' - ${v.site}',
                                      style: TextStyle(
                                        color: Colors.white30,
                                        fontSize: 13.0,
                                        fontFamily: 'MontserratRegular',
                                        fontStyle: FontStyle.italic
                                      ),
                                      textScaleFactor: 1.0,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Container(
                                height: 65.0,
                                width: MediaQuery.of(context).size.width * .6,
                                child: Center(
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 8.0, right: 8.0),
                                    child: Text(
                                      v.name,
                                      style: TextStyle(
                                          fontSize: 14.0,
                                          fontFamily: 'RalewayRegular',
                                          color: Colors.white),
                                      textAlign: TextAlign.left,
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 4,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 5.0,
                              ),
                              GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                VideosWebViewPage(
                                                    urlVideo: v.key)));
                                  },
                                  child: ClipRRect(
                                      borderRadius: BorderRadius.circular(8.0),
                                      child: Container(
                                        height: 40.0,
                                        width:
                                            MediaQuery.of(context).size.width *
                                                .22,
                                        color: Color.fromRGBO(255, 0, 57, 1),
                                        child: Center(
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Icon(
                                                Icons.play_arrow,
                                                color: Colors.white,
                                                size: 30.0,
                                              ),
                                            ],
                                          ),
                                        ),
                                      )))
                            ],
                          ),
                        ],
                      ),
                    ),)
                .toList(),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color.fromRGBO(26, 27, 31, 1),
        appBar: new AppBar(
          backgroundColor: Colors.transparent,
          title: Text(
            'Videos de ${widget.name}',
            style: TextStyle(
              fontSize: 15.0,
              fontFamily: 'MontserratRegular',
            ),
            overflow: TextOverflow.ellipsis,
          ),
        ),
        body: ListView(children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 8.0, bottom: 12.0, top: 4.0),
            child: Text(
              'Videos en Español',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.0,
                  fontFamily: 'MontserratMedium'),
              textScaleFactor: 1.0,
            ),
          ),
          _buildListVideos(videosModelEsp),
          Divider(
            color: Colors.white30,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8.0, bottom: 12.0, top: 4.0),
            child: Text(
              'Videos en Ingles',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 18.0,
                  fontFamily: 'MontserratMedium'),
            ),
          ),
          _buildListVideos(videosModelIng),
        ]));
  }
}
