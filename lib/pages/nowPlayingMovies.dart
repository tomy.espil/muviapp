import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:muvi_app/helpers/loader.dart';
import 'package:muvi_app/models/movieModel.dart';
import 'package:muvi_app/models/tmdb.dart';
import 'package:muvi_app/pages/movieDetails_page.dart';

class NowPlayingMovies extends StatefulWidget {
  _NowPlayingMoviesState createState() => _NowPlayingMoviesState();
}

class _NowPlayingMoviesState extends State<NowPlayingMovies> {
  Movie nowPlayingMovies;

  int _pageNumber = 1;
  int _totalItems = 3;

  @override
  void initState() {
    super.initState();
    _fetchNowPlayingMovies();
  }

  void _fetchNowPlayingMovies() async {
    var response = await http.get(
        '${Tmdb.baseUrl}now_playing?api_key=${Tmdb.apiKey}&language=es&page=$_pageNumber&region=AR');
    var decodeJson = jsonDecode(response.body);
    nowPlayingMovies == null
        ? nowPlayingMovies = Movie.fromJson(decodeJson)
        : nowPlayingMovies.movies.addAll(Movie.fromJson(decodeJson).movies);
    if (!mounted) return;
    setState(() {
      _totalItems = nowPlayingMovies.movies.length;
    });
  }

  Widget _buildMovieListItem(Movies movieItem) => Container(
        height: 170.0,
        child: Padding(
          padding: const EdgeInsets.only(left: 10.0, right: 10.0, top: 15.0),
          child: Stack(
            children: <Widget>[
              Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                      child: Container(
                        height: 1.0,
                        width: double.infinity,
                        color: Color.fromRGBO(255, 0, 57, .8),
                      ),
                    ),
                    Card(
                      color: Color.fromRGBO(14, 15, 18, 1),
                      elevation: 5.0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      child: Stack(
                        children: <Widget>[
                          Container(
                            height: 110.0,
                            child: InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => MovieDetailPage(
                                              movie: movieItem,
                                            )));
                              },
                            ),
                          ),
                          Positioned(
                              left: 120.0,
                              child: Container(
                                width: 190.0,
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      movieItem.title,
                                      style: TextStyle(
                                          fontFamily: 'MontserratBold',
                                          fontSize: 16.0,
                                          color: Colors.white),
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                    /* SizedBox(
                                      height: 3.0,
                                    ),
                                    Container(
                                      height: 1.0,
                                      width: 40.0,
                                      color: Color.fromRGBO(255, 0, 57, 1),
                                    ), */
                                    SizedBox(
                                      height: 7.0,
                                    ),
                                    Text(
                                        "${DateFormat('dd-MM-yyyy').format(DateTime.parse(movieItem.releaseDate))}",
                                        style: TextStyle(
                                          fontSize: 13.0,
                                          fontFamily: 'RalewayLight',
                                          color: Colors.grey,
                                        )),
                                    SizedBox(
                                      height: 7.0,
                                    ),
                                    Text(
                                      movieItem.overview,
                                      style: TextStyle(
                                        fontSize: 13.0,
                                        fontFamily: 'RalewayLight',
                                        color: Colors.white,
                                      ),
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 3,
                                    ),
                                  ],
                                ),
                              ))
                        ],
                      ),
                    ),
                  ]),
              Positioned(
                left: 15.0,
                bottom: 12.0,
                child: Container(
                  height: 140.0,
                  width: 100.0,
                  child: ClipRRect(
                    borderRadius: new BorderRadius.circular(4.0),
                    child: movieItem.posterPath != null
                        ? Image.network(
                            '${Tmdb.baseImageUrl}w342${movieItem.posterPath}',
                            fit: BoxFit.cover,
                          )
                        : Image.asset(
                            'assets/emptyfilmposter.jpg',
                            fit: BoxFit.cover,
                          ),
                  ),
                ),
              ),
            ],
          ),
        ),
      );

  Widget _buildCardsList(Movie movie) => ListView.builder(
        itemCount: _totalItems,
        itemBuilder: (BuildContext context, int index) {
          if (index >= movie.movies.length - 1) {
            _pageNumber++;
            _fetchNowPlayingMovies();
          }
          return Padding(
            padding: EdgeInsets.only(left: 4.0, right: 4.0),
            child: _buildMovieListItem(movie.movies[index]),
          );
        },
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color.fromRGBO(26, 27, 31, 1),
        appBar: new AppBar(
          backgroundColor: Color.fromRGBO(18, 19, 23, 1),
          elevation: 0.0,
          centerTitle: true,
          title: Text(
            'EN CARTELERA',
            style: TextStyle(
              fontFamily: 'RalewayMedium',
              fontSize: 20.0,
            ),
          ),
        ),
        body: nowPlayingMovies == null
            ? Center(
                child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Loader(),
                  Text(
                    'Cargando lista de peliculas',
                    style: TextStyle(
                        fontFamily: 'RalewayMedium',
                        color: Colors.white,
                        fontSize: 16.0),
                  )
                ],
              ))
            : _buildCardsList(
                nowPlayingMovies,
              ));
  }
}
